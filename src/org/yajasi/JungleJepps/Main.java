package org.yajasi.JungleJepps;

import java.io.IOException;
import java.sql.SQLException;

import org.yajasi.JungleJepps.db.DatabaseConnection;
import org.yajasi.JungleJepps.db.DatabaseException;
import org.yajasi.JungleJepps.db.DatabaseManager;
import org.yajasi.JungleJepps.ui.NewJFrame;
import org.yajasi.JungleJepps.ui.NewUI;

public class Main {

	public static void main(String[] args) throws SQLException, DatabaseException {
	    
		try {
			System.out.println("Running main...");

			
			DatabaseConnection db = DatabaseManager.getDatabase();
			
			Thread.sleep(4000);
			NewUI.main(args);
			
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}

}
