/////////////////////////////////////////////////////////////////////////
// Author: Jesse Sjoblom
// File: OperationsDatabase.java
// Class: org.yajasi.JungleJepps.db.OperationsDatabase
//
// Target Platform: Java Virtual Machine 
// Development Platform: Windows 8
// Development Environment: Netbeans 7.3.1
// 
// Project: Jungle Jepps - Desktop
// Copyright 2013 YAJASI. All rights reserved. 
//
/////////////////////////////////////////////////////////////////////////
package org.yajasi.JungleJepps.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.yajasi.JungleJepps.Field;
import org.yajasi.JungleJepps.Runway;

class OperationsDatabase implements DatabaseConnection {
	
	private SettingsManager settingsMgr;
	private Connection connection;
	private DateFormat ts = new SimpleDateFormat("yyyyMMddHHmmss");
	
	/**
         * Makes a read only connection to a 3rd party DB
         * @param settings
         * @param thirdPartyDbNumber
         * @throws DatabaseException 
         */
        public OperationsDatabase(SettingsManager settings, int thirdPartyDbNumber) throws DatabaseException{
		this.settingsMgr = settings;
		
                
                String dbDriverClass = settingsMgr.get(Settings.OPERATIONS_JDBC_CLASS_PATH).split("$")[thirdPartyDbNumber];
		String dbUrl = settingsMgr.get(Settings.OPERATIONS_JDBC_URI).split("$")[thirdPartyDbNumber];
		
		try{
	        // Load JDBC class into runtime
			Class.forName( dbDriverClass );
	
			// Request class from Driver Manager
			this.connection = DriverManager.getConnection(dbUrl);
			
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new DatabaseException(e); //Rethrow exception using e
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DatabaseException(e); //Rethrow exception using e
		}
		
	}
	
	/**
         * builds and returns a runway containing all read only data needed form the 3rd party db.
         * @param runwayId
         * @param aircraftId
         * @return
         * @throws DatabaseException 
         */
	public Runway getRunway(String runwayId, String aircraftId) throws DatabaseException{
                return Runway.initialize(getTempRunway(runwayId, aircraftId));
	}
        
        /**
         * builds and returns a runwayBuilder containing all read only data needed from the 3rd party db.
         * @param runwayId
         * @param aircraftId
         * @return
         * @throws DatabaseException 
         */
         public RunwayBuilder getTempRunway(String runwayId, String aircraftId) throws DatabaseException{
                RunwayBuilder tempRunway = new RunwayBuilder();
		ResultSet rs;
		
		String sql = String.format("SELECT * FROM %s WHERE %s = '%s' AND %s = '%s'", 
				settingsMgr.get(Settings.OPERATIONS_TABLE_NAME),
				settingsMgr.getOverrideColumn(Field.AIRCRAFT_IDENTIFIER),
				aircraftId,
				settingsMgr.getOverrideColumn(Field.RUNWAY_IDENTIFIER),
				runwayId);

		try{
			rs = connection.createStatement().executeQuery(sql);
	
			while(rs.next()){
				for(Field f : Field.values()){
					if( settingsMgr.isFieldOverridden(f) ){
						tempRunway.put(f, rs.getString( settingsMgr.getOverrideColumn(f) ));
					}
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
			throw new DatabaseException(e);
		}
		
		return tempRunway;
         }

        /**
         * Gets all Aircraft types form the DB
         * @return Aircraft types in an array;
         * @throws DatabaseException 
         */
	@Override
	public String[] getAllAircraftIds() throws DatabaseException {
		ArrayList<String> results = new ArrayList<String>();
                String sql = String.format("SELECT %s FROM %s", 
                                           settingsMgr.getOverrideColumn(Field.AIRCRAFT_IDENTIFIER),
                                           settingsMgr.get(Settings.OPERATIONS_TABLE_NAME));
                ResultSet rs; 
                try{
                    rs = connection.createStatement().executeQuery(sql);
                    while(rs.next()){
                        results.add(rs.getString(settingsMgr.getOverrideColumn(Field.AIRCRAFT_IDENTIFIER)));
                    }
                }
                catch(SQLException e){
			e.printStackTrace();
			throw new DatabaseException(e);
		}
                
		return results.toArray(new String[results.size()]);
	}

        /**
         * gets runway IDs from the DB associated with an aircraft ID
         * @param aircraftId
         * @return runway IDs in an array
         * @throws DatabaseException 
         */
	@Override
	public String[] getAllRunwayIds(String aircraftId) throws DatabaseException {
		ArrayList<String> results = new ArrayList<String>();
                String sql = String.format("SELECT %s FROM %s "
                                         + "WHERE %s = '%s'", 
                                         settingsMgr.getOverrideColumn(Field.RUNWAY_IDENTIFIER),
                                         settingsMgr.get(Settings.OPERATIONS_TABLE_NAME), 
                                         settingsMgr.getOverrideColumn(Field.AIRCRAFT_IDENTIFIER),
                                         aircraftId);
                ResultSet rs; 
                try{
                    rs = connection.createStatement().executeQuery(sql);
                    while(rs.next()){
                        results.add(rs.getString(settingsMgr.getOverrideColumn(Field.RUNWAY_IDENTIFIER)));
                    }
                }
                catch(SQLException e){
			e.printStackTrace();
			throw new DatabaseException(e);
		}
                
		return results.toArray(new String[results.size()]);
	}

        /**
         * gets all runway IDs
         * @return runway IDs in an array
         * @throws DatabaseException 
         */
	@Override
	public String[] getAllRunwayIds() throws DatabaseException {
		ArrayList<String> results = new ArrayList<String>();
                String sql = String.format("SELECT %s FROM %s", 
                                         settingsMgr.getOverrideColumn(Field.RUNWAY_IDENTIFIER),
                                         settingsMgr.get(Settings.OPERATIONS_TABLE_NAME));
                ResultSet rs; 
                try{
                    rs = connection.createStatement().executeQuery(sql);
                    while(rs.next()){
                        results.add(rs.getString(settingsMgr.getOverrideColumn(Field.RUNWAY_IDENTIFIER)));
                    }
                }
                catch(SQLException e){
			e.printStackTrace();
			throw new DatabaseException(e);
		}
                
		return results.toArray(new String[results.size()]);
	}

        /**
         * This is part of the interface that is not used with the 3rd party db
         * @param runway
         * @return false
         * @throws DatabaseException 
         */
	@Override
	public boolean updateRunway(Runway runway) throws DatabaseException {
		System.out.println("Update on third party Database not permited");
		return false;
	}
	
	/**
	 * returns requested number of logs newest first
	 */
	@Override
public String[][] getLog(String runwayId, String aircraftId, int requested) throws DatabaseException{
    	
    	ResultSet rs;
    	ArrayList<String[]> resultsSetup = new ArrayList<String[]>();
    	String[][] results;
    	String[] row;
    	String sql = String.format("SELECT * FROM log WHERE AIRCRAFT_IDENTIFIER = '%s' AND RUNWAY_IDENTIFIER = '%s' ORDER BY time DESC", aircraftId, runwayId);
    	
    	
    	try{
			rs = connection.createStatement().executeQuery(sql);
			
			int I = 0;
			while(rs.next() && I > requested){
				row = new String[5];
				row[0] = rs.getString("AIRCRAFT_IDENTIFIER");
				row[1] = rs.getString("RUNWAY_IDENTIFIER");
				row[2] = rs.getString("user");
				row[3] = rs.getString("field_updated");
				row[4] = rs.getString("time");
				resultsSetup.add(row);
				I++;
			}
			results = new String[I][9];
			for(int J = 0; J < I; J++){
				results[J] = resultsSetup.get(J);
			}
		}catch(SQLException e){
			e.printStackTrace();
			throw new DatabaseException(e);
		}
    	        	
    	return results;
    }

        /**
         * Closes the DB connection. Use to avoided leaks
         * @return
         * @throws DatabaseException 
         */
	@Override
	public boolean close() throws DatabaseException {
		try{
                    connection.close();
                }
                catch(SQLException e){
			e.printStackTrace();
			throw new DatabaseException(e);
		}
		return false;
	}
        
        /** This method is an example of how to query and work in a JDBC Context
         * Just a Test method. This method runs through each of the features of the class
         * in order to make sure everything is working.
         * @param args
         * @throws DatabaseException 
         */
        public static void main(String[] args) throws DatabaseException{
            System.out.println("This is the main method of the OperationsJdbcCource Class\n");
            SettingsManager settings = SettingsManager.getInstance();
            //settings for mysql
            settings.setValue(Settings.PRIMARY_JDBC_CLASS_PATH, "com.mysql.jdbc.Driver"); 
            settings.setValue(Settings.PRIMARY_JDBC_URI, "jdbc:mysql://localhost/JJDB?user=jepps&password=jepps");
            settings.setValue(Settings.OPERATIONS_JDBC_CLASS_PATH, "com.mysql.jdbc.Driver");
            settings.setValue(Settings.OPERATIONS_JDBC_URI, "jdbc:mysql://localhost/JJDB3?user=jepps&password=jepps");
            settings.setOverrideColumn(Field.AIRCRAFT_IDENTIFIER, "craft");
            settings.setOverrideColumn(Field.RUNWAY_IDENTIFIER, "RUNWAY_ID");
            settings.setOverrideColumn(Field.PDF_PATH, "PDF");
            settings.setValue(Settings.OPERATIONS_TABLE_NAME, "a_id");

            OperationsDatabase db = new OperationsDatabase(settings, 0);
            
            String[] aircraftIds, runwayIds;
            Runway runway;

            ///getting runway and aircraft IDs
            aircraftIds = db.getAllAircraftIds();
            System.out.println("number of aircraft= " + aircraftIds.length);
            runwayIds = db.getAllRunwayIds( aircraftIds[0] );
            System.out.println("number of runway= " + runwayIds.length);
            runway = db.getRunway(runwayIds[0], aircraftIds[0]); 
            
            System.out.print("\nAircraft IDs: ");
            for(String aid : aircraftIds)
                    System.out.print(aid + ", ");

            System.out.print("\nRunway IDs: ");
            for(String rid : runwayIds)
                    System.out.print(rid + ", ");
                       
            ///row display
            runway = db.getRunway(runwayIds[0], aircraftIds[0]);
            System.out.print("\n\n-----" + runwayIds[0] + "---" + aircraftIds[0] + "---Field Printouts-----\n");
            for(Field f : runway.keySet())
                    System.out.println(f.toString() + ": " + runway.get(f) );
            
            db.close();
            
        }

		@Override
		public void publish(Runway runway) throws DatabaseException,
			//publish is not used on operations databases	
			SQLException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String[][] getLog(String runwayId, String aircraftId,
				int requested, int type) throws DatabaseException {
			// TODO Auto-generated method stub
			return null;
		}

}
