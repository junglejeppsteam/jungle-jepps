/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yajasi.JungleJepps.db;

import com.sun.rowset.CachedRowSetImpl;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.rowset.RowSetMetaDataImpl;

/**
 *
 * @author jesse_000
 */
public class DBAmultiple extends DataBaseAccess{
    
    DataBaseAccess DBAExternal;
    CachedRowSetImpl FieldExternal;
    //select field, overridden from tbl_meta
    
    //select longitude, latitude from tbl_runway
    
    DBAmultiple(String type, String location){
        super(type,location);
        try {
            CachedRowSetImpl crs = super.query("select VALUE, VALUE_OPT from TBL_SETTINGS where FIELD=\"OPERATION_DATABASE\"");
            if(crs.next()){
                System.out.println("external DB: " + crs.getString("VALUE") + " : " + crs.getString("VALUE_OPT"));
            }
            DBAExternal = new DBAsingle(crs.getString("VALUE"),crs.getString("VALUE_OPT"));
            FieldExternal = super.query("select FIELD, EXTERNAL_COLUMN from TBL_META where OVERRIDDEN=\"true\"");
            if(FieldExternal.next()){
                System.out.println(FieldExternal.getString("FIELD") + " , " + FieldExternal.getString("EXTERNAL_COLUMN"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBAmultiple.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
     public CachedRowSetImpl query(String sql){
        try {
            //String[] sqlSplit = sql.split("(?<!,)\\s");
            String[] sqlSplit = sql.split("select|from|where");
            String[] select,from,where;
            ArrayList<String> extSelect = new ArrayList<String>();
            ArrayList<String> extFrom = new ArrayList<String>();
            ArrayList<String> extWhere = new ArrayList<String>();
            ArrayList<String> intSelect = new ArrayList<String>();
            ArrayList<String> intFrom = new ArrayList<String>();
            ArrayList<String> intWhere = new ArrayList<String>();
            String extSQL = "";
            String intSQL = "";
            CachedRowSetImpl extCrs;
            CachedRowSetImpl intCrs;
            CachedRowSetImpl mrgCrs;
            
            //testing
            System.out.print("\nsqlSplit : ");
            for(String s: sqlSplit){
                s = s.trim();
                System.out.print(s + " | ");
            }
            select = sqlSplit[1].split(",");
            System.out.print("\nselect : ");
            for(String s: select){
                s = s.trim();
                System.out.print(s + " | ");
            }
            from = sqlSplit[2].split(",");
            System.out.print("\nfrom : ");
            for(String s: from){
                s = s.trim();
                System.out.print(s + " | ");
            }
            System.out.print("\nwhere : ");
            if(sqlSplit.length > 3){
               where = sqlSplit[3].split(",");            
               for(String s: where){
                   s = s.trim();
                   System.out.print(s + " | ");
               }
            }
            System.out.print("\n");
            // end testing
            // build internal and external filed lists
            //selects = buildSelectFiledLists(FieldExternal);
            //extSelect = selects[0];
            //extSelect = selects[0];
            boolean found = false;
            for(String s: select){
                found = false;
                System.out.println(s);
                FieldExternal.beforeFirst();
                while(FieldExternal.next()){
                    System.out.print("    " + FieldExternal.getString("FIELD"));
                    if(FieldExternal.getString("FIELD").trim().compareToIgnoreCase(s.trim()) == 0){
                        System.out.print("*");
                        extSelect.add(FieldExternal.getString("EXTERNAL_COLUMN").trim());
                        found = true;
                    }
                    System.out.print("\n");
                }
                if(found == false){
                    intSelect.add(s.trim());
                }
            }
            
            if(sqlSplit[2].contains("join")){
                throw new UnsupportedOperationException("Join not supported yet.");
            }
            //////////////////////////////////////////
            //generating external sql select statment
            boolean first = true;
            for(String s: extSelect){
                if(first){
                    extSQL = "select " + s;
                    first = false;
                }
                else{
                    extSQL = extSQL + ", " + s;
                }
            }
            //getting external table name
            extCrs = super.query("select VALUE from TBL_SETTINGS where FIELD=\"OPERATION_TABLE\"");
            extCrs.next();
            extSQL = extSQL + " from " + extCrs.getString("VALUE");
            System.out.println("extSQL: " + extSQL);
            /////////////////////////////////////////
            first = true;
            for(String s: intSelect){
                if(first){
                    intSQL = "select " + s;
                    first = false;
                }
                else{
                    intSQL = intSQL + ", " + s;
                }
            }
            intSQL = intSQL + " from " + sqlSplit[2];
            if(sqlSplit.length > 3){// does nothering atm if where is found
                intSQL = intSQL + " where " + sqlSplit[3];
            }
            System.out.println("intSQL: " + intSQL);
            /////////////////////////////////////////// 
            extCrs = DBAExternal.query(extSQL);
            intCrs = super.query(intSQL);
            //////////////////////////////////////////
            // testing code
            int I, J;
            String out = "";
            J = extCrs.getMetaData().getColumnCount();
            while(extCrs.next()){
                I = 1;
                while(I <= J){
                    if(I == 1){
                       out = out + extCrs.getString(I);
                    }else{
                        out = out + " | " + extCrs.getString(I);
                    }
                    I++;
                }
                out = out + " \n ";
            }
            System.out.println(out);
            out = "";
            J = intCrs.getMetaData().getColumnCount();
            while(intCrs.next()){
                I = 1;
                while(I <= J){
                    if(I == 1){
                       out = out + intCrs.getString(I);
                    }else{
                        out = out + " | " + intCrs.getString(I);
                    }
                    I++;
                }
                out = out + " \n ";
            }
            System.out.println(out);
            
            //build new crs for merge
            intCrs.beforeFirst();
            extCrs.beforeFirst();
            RowSetMetaDataImpl rsMDI = new RowSetMetaDataImpl();
            int columnNumber = intCrs.getMetaData().getColumnCount() + extCrs.getMetaData().getColumnCount();
            int rowNumber = intCrs.size();
            rsMDI.setColumnCount(columnNumber);
            for(int c = 1 ; c <= columnNumber ; c++){
                if(c <= intCrs.getMetaData().getColumnCount()){
                    System.out.println(intCrs.getMetaData().getColumnName(c));
                    rsMDI.setColumnName(c, intCrs.getMetaData().getColumnName(c));
                    rsMDI.setColumnType(c, Types.VARCHAR);
                }
                else{
                    System.out.println(extCrs.getMetaData().getColumnName(c-intCrs.getMetaData().getColumnCount()));
                    rsMDI.setColumnName(c, extCrs.getMetaData().getColumnName(c-intCrs.getMetaData().getColumnCount()));
                    rsMDI.setColumnType(c, Types.VARCHAR);
                }    
            }
            System.out.println("rsMDI Column names: ");
            for(int c = 1; c <= rsMDI.getColumnCount(); c++){
                System.out.println("   " + rsMDI.getColumnName(c));
            }
            mrgCrs = new CachedRowSetImpl();
            mrgCrs.setMetaData(rsMDI);
            //populate merge crs
            while(intCrs.next()){                
                extCrs.next();
                mrgCrs.moveToInsertRow();
                for(int c = 1 ; c <= columnNumber ; c++){
                    if(c <= intCrs.getMetaData().getColumnCount()){
                        System.out.println("intCrs c = " + c + ": " + intCrs.getString(c));
                        mrgCrs.updateString(c, intCrs.getString(c));
                    }
                    else{
                        System.out.println("extCrs c = " + c + ": " + extCrs.getString(c-intCrs.getMetaData().getColumnCount()));
                        mrgCrs.updateString(c, extCrs.getString(c-intCrs.getMetaData().getColumnCount()));
                    }    
                }
                mrgCrs.insertRow();
                //mrgCrs.moveToInsertRow();
            }
            mrgCrs.moveToCurrentRow();
            mrgCrs.beforeFirst();
            //mrgCrs.acceptChanges();
            // testing
            System.out.println("mrgCrs:");
            out = "";
            J = mrgCrs.getMetaData().getColumnCount();
            System.out.println(J);
            while(mrgCrs.next()){
                I = 1;
                while(I <= J){
                    if(I == 1){
                       out = out + mrgCrs.getString(I);
                    }else{
                        out = out + " | " + mrgCrs.getString(I);
                    }
                    I++;
                }
                out = out + " \n ";
            }
            System.out.println(out);
            mrgCrs.beforeFirst();
            return mrgCrs;
        } catch (SQLException ex) {
            Logger.getLogger(DBAmultiple.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
     }
     
     public String buildSelect(ArrayList<String> list){//
         String out = null;
         boolean first = true;
            for(String s: list){
                if(first){
                    out = "select " + s;
                    first = false;
                }
                else{
                    out = out + ", " + s;
                }
            }
        return out;
     }
}
