/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.yajasi.JungleJepps.db;

import com.sun.rowset.CachedRowSetImpl;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.RowSet;
import javax.sql.rowset.CachedRowSet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author JesseSjoblom
 */
public class DataBaseAccess {

	String strCon;

	public DataBaseAccess(String type, String location) {
		strCon = "jdbc:" + type + ":" + location;
	}

	public CachedRowSetImpl query(String sql) {
		try {
			CachedRowSetImpl crs = new CachedRowSetImpl();
			Connection con = DriverManager.getConnection(strCon);
			crs.populate(con.createStatement().executeQuery(sql));
			con.close();
			return crs;

		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return null;
		}
	}

	public boolean update(String sql) {
		try {
			Connection con = DriverManager.getConnection(strCon);
			con.createStatement().executeUpdate(sql);
			con.close();
			return true;
		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return false;
		}
	}

	public CachedRowSetImpl getField(String field, String AID, String RID) {
		try {
			String sql = String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK, LINK_TABLE From TBL_META Where FIELD=\"%s\"",
							field);
			CachedRowSetImpl metaData = query(sql);
			if (metaData.next()) {
				// sql =
				// String.format("Select DISTINCT %s, t.%s From %s as t, TBL_EVENT as e, TBL_RUNWAY as r, TBL_AIRCRAFT as a Where a.AIRCRAFT_IDENTIFIER=\"%s\" and r.RUNWAY_IDENTIFIER=\"%s\" and e.RUNWAY_ID = r.ID and e.AIRCRAFT_ID = a.ID and t.ID=%s",
				// metaData.getString("LINK"),
				// metaData.getString("INTERNAL_COLUMN"),
				// metaData.getString("INTERNAL_TABLE"), AID, RID,
				// metaData.getString("LINK"));
				if (metaData.getString("LINK_TABLE").compareToIgnoreCase(
						"TBL_EVENT") == 0) {
					sql = String
							.format("Select %s From TBL_EVENT as e, TBL_RUNWAY as r, TBL_AIRCRAFT as a "
									+ "Where a.AIRCRAFT_IDENTIFIER=\"%s\" and r.RUNWAY_IDENTIFIER=\"%s\" and e.RUNWAY_ID = r.ID "
									+ "and e.AIRCRAFT_ID = a.ID",
									metaData.getString("INTERNAL_COLUMN"), AID,
									RID, metaData.getString("LINK"));
					// System.out.println("getField: " + sql);
					return query(sql);
				} else {
					sql = String
							.format("Select t.%s From %s as t, TBL_EVENT as e, TBL_RUNWAY as r, TBL_AIRCRAFT as a "
									+ "Where a.AIRCRAFT_IDENTIFIER=\"%s\" and r.RUNWAY_IDENTIFIER=\"%s\" and e.RUNWAY_ID = r.ID "
									+ "and e.AIRCRAFT_ID = a.ID and t.id=%s",
									metaData.getString("INTERNAL_COLUMN"),
									metaData.getString("INTERNAL_TABLE"), AID,
									RID, metaData.getString("LINK"));
					// System.out.println("getField: " + sql);
					return query(sql);
				}

			} else {
				System.out.println("getField: Field: " + field
						+ " is not valid.");
				return null;
			}
		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return null;
		}
	}

	public boolean setField(String field, String AID, String RID, String data) {
		try {
						
			
			CachedRowSetImpl oldData = getField(field, AID, RID);
			CachedRowSetImpl metaData = query(String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK, LINK_TABLE, IS_AN_ID From TBL_META Where FIELD=\"%s\"",
							field));
			int id;
			String sql;
			if (oldData.next()) {
				// System.out.println("setField: " +
				// oldData.getString(metaData.getString("INTERNAL_COLUMN")));
				if (metaData.next()) {
					
					
					
					String d = oldData
							.getString(metaData.getString("INTERNAL_COLUMN"));
							
					
					if (d != null && d.compareToIgnoreCase(data) == 0) {
						System.out.println("setField: date already set");
						return true;
					} else {// we need to make a change

						if (metaData.getBoolean("IS_AN_ID") == true) {// if
																		// field
																		// is an
																		// ID we
																		// cant
																		// just
																		// change
																		// it.
																		// We
																		// need
																		// to
																		// find
																		// or
																		// build
																		// a new
																		// record
																		// to
																		// match
																		// data
							id = getIDForData(field, data);
							System.out.println("setField: ID= " + id);
							if (id == 0) {// a record for this id does not exist
								if (field
										.compareToIgnoreCase("RUNWAY_IDENTIFIER") == 0) {
									id = addToDropDown(field, AID, data);
								} else {
									id = addToDropDown(field, data);
								}
							}
							System.out.println("setField: ID= " + id);
							if (metaData.getString("LINK_TABLE")
									.compareToIgnoreCase("TBL_RUNWAY") == 0) {
								sql = String
										.format("Update %s Set %s = \'%s\' Where ID In (Select RUNWAY_ID From TBL_EVENT Where AIRCRAFT_ID = \"%s\" and RUNWAY_ID = \"%s\")",
												metaData.getString("LINK_TABLE"),
												metaData.getString("LINK"),
												id,
												getIDForData(
														"AIRCRAFT_IDENTIFIER",
														AID),
												getIDForData(
														"RUNWAY_IDENTIFIER",
														RID));
							} else {
								sql = String
										.format("Update %s Set %s = \'%s\' Where AIRCRAFT_ID = \"%s\" and RUNWAY_ID = \"%s\"",
												metaData.getString("LINK_TABLE"),
												metaData.getString("LINK"),
												id,
												getIDForData(
														"AIRCRAFT_IDENTIFIER",
														AID),
												getIDForData(
														"RUNWAY_IDENTIFIER",
														RID));
							}
							System.out.println("setField: " + sql);
							update(sql);
							return true;
						} else {// field is not an ID so it can be edited
							sql = String
									.format("Update %s Set %s = \"%s\" Where ID In "
											+ "(Select e.%s From TBL_EVENT as e, TBL_RUNWAY as r, TBL_AIRCRAFT as a "
											+ "Where a.ID = e.AIRCRAFT_ID and r.ID = e.RUNWAY_ID and AIRCRAFT_IDENTIFIER = \"%s\" and RUNWAY_IDENTIFIER = \"%s\")",
											metaData.getString("INTERNAL_TABLE"),
											metaData.getString("INTERNAL_COLUMN"),
											data, metaData.getString("LINK"),
											AID, RID);
							System.out.println("setField: " + sql);
							update(sql);
							return true;
						}
					}
				} else {
					System.out.println("setField: Field: " + field
							+ "not valid.");
					return false;
				}
			} else {
				System.out
						.println("setField: Field for this relationship does not exist");
			}
			return false;
		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return false;
		}
	}

	public boolean HLPossible(String field, String AID, String RID) {
		try {
			String sql = String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK, HL_POSSIBLE From TBL_META Where FIELD=\"%s\"",
							field);
			CachedRowSetImpl metaData = query(sql);
			if (metaData.next()) {
				if (metaData.getBoolean("HL_POSSIBLE")) {
					// sql =
					// String.format("Select DISTINCT %s, t.%s From %s as t, TBL_EVENT as e, TBL_RUNWAY as r, TBL_AIRCRAFT as a Where a.AIRCRAFT_IDENTIFIER=\"%s\" and r.RUNWAY_IDENTIFIER=\"%s\" and e.RUNWAY_ID = r.ID and e.AIRCRAFT_ID = a.ID and t.ID=%s",
					// metaData.getString("LINK"),
					// metaData.getString("INTERNAL_COLUMN"),
					// metaData.getString("INTERNAL_TABLE"), AID, RID,
					// metaData.getString("LINK"));
					sql = String
							.format("Select t.%s_HL From %s as t, TBL_EVENT as e, TBL_RUNWAY as r, TBL_AIRCRAFT as a "
									+ "Where a.AIRCRAFT_IDENTIFIER=\"%s\" and r.RUNWAY_IDENTIFIER=\"%s\" and e.RUNWAY_ID = r.ID "
									+ "and e.AIRCRAFT_ID = a.ID and t.ID=%s",
									metaData.getString("INTERNAL_COLUMN"),
									metaData.getString("INTERNAL_TABLE"), AID,
									RID, metaData.getString("LINK"));
					System.out.println("getField: " + sql);
					return query(sql).getBoolean("HL_POSSIBLE");
				} else {
					System.out
							.println("HLPossible: This Field cannot be Highlighted");
					return false;
				}
			} else {
				System.out.println("getField: Field is not valid.");
				return false;
			}
		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return false;
		}
	}

	public CachedRowSetImpl getDropDown(String field) {
		try {
			CachedRowSetImpl metaData = query(String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK From TBL_META Where FIELD=\"%s\"",
							field));
			if (metaData.next()) {
				CachedRowSetImpl dropDown = query(String.format(
						"Select %s from %s",
						metaData.getString("INTERNAL_COLUMN"),
						metaData.getString("INTERNAL_TABLE")));
				return dropDown;
			} else {
				System.out.println("getDropDown: Field not valid");
				return null;
			}

		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return null;
		}
	}

	public CachedRowSetImpl getDropDown(String field, String AID) {
		try {
			CachedRowSetImpl metaData = query(String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK From TBL_META Where FIELD=\"%s\"",
							field));
			if (metaData.next()) {
				CachedRowSetImpl dropDown = query(String
						.format("Select %s from %s as t, TBL_EVENT as e, TBL_AIRCRAFT as a Where e.AIRCRAFT_ID = a.ID and e.%s=t.ID and a.AIRCRAFT_IDENTIFIER = \"%s\"",
								metaData.getString("INTERNAL_COLUMN"),
								metaData.getString("INTERNAL_TABLE"),
								metaData.getString("LINK"), AID));
				return dropDown;
			} else {
				System.out.println("getDropDown: Field not valid");
				return null;
			}

		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return null;
		}
	}

	public int addToDropDown(String field, String newData) {
		try {
			CachedRowSetImpl metaData = query(String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK From TBL_META Where FIELD=\"%s\"",
							field));
			if (metaData.next()) {
				boolean found = false;// checking to see if newData is already
										// added
				CachedRowSetImpl oldData = getDropDown(field);
				while (oldData.next()) {
					if (oldData
							.getString(metaData.getString("INTERNAL_COLUMN"))
							.compareToIgnoreCase(newData) == 0) {
						found = true;// this is old data
						break;
					}
				}
				if (found == false) {
					String sql = String.format(
							"Insert into %s(%s) Values(\"%s\")",
							metaData.getString("INTERNAL_TABLE"),
							metaData.getString("INTERNAL_COLUMN"), newData);
					update(sql);
					return getIDForData(field, newData);
				} else {
					System.out
							.println("addToDropDown: newData already in table");
					return getIDForData(field, newData);
				}
			} else {
				System.out.println("addToDropDown: Field not valid");
				return 0;
			}
		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return 0;
		}
	}

	public int addToDropDown(String field, String AID, String newData) {// This
																		// will
																		// break
																		// if
																		// its
																		// not
																		// used
																		// with
																		// field
																		// =
																		// "RUNWAY_IDENTIFIER";
																		// However,
																		// it
																		// should
																		// be
																		// easy
																		// to
																		// modify
																		// to
																		// alow
																		// for
																		// other
																		// dependent
																		// Drop
																		// Downs
																		// if
																		// that
																		// is
																		// ever
																		// needed.
		try {
			CachedRowSetImpl metaData = query(String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK From TBL_META Where FIELD=\"%s\"",
							field));
			if (metaData.next()) {
				boolean found = false;// checking to see if newData is already
										// added
				CachedRowSetImpl oldData = getDropDown(field);
				while (oldData.next()) {
					if (oldData
							.getString(metaData.getString("INTERNAL_COLUMN"))
							.compareToIgnoreCase(newData) == 0) {
						found = true;// this is old data
						break;
					}
				}
				if (found == false) {
					System.out.println("addToDropDown: newData being added");
					String sql = String.format(
							"Insert into %s(%s) Values(\"%s\")",
							metaData.getString("INTERNAL_TABLE"),
							metaData.getString("INTERNAL_COLUMN"), newData);
					System.out.println("addToDropDown: " + sql);
					update(sql);

				} else {
					System.out
							.println("addToDropDown: newData already in table");
					found = false;// reusing found and old data to check for
									// existing relationship
					oldData = query(String
							.format("Select t.%s From %s as t, TBL_AIRCRAFT as a, TBL_EVENT as e Where a.ID = e.AIRCRAFT_ID and t.ID = e.%s and a.AIRCRAFT_IDENTIFIER = \"%s\"",
									metaData.getString("INTERNAL_COLUMN"),
									metaData.getString("INTERNAL_TABLE"),
									metaData.getString("LINK"), AID));
					while (oldData.next()) {
						if (oldData.getString(
								metaData.getString("INTERNAL_COLUMN"))
								.compareToIgnoreCase(newData) == 0) {
							found = true;// this is old data
						}
					}
				}
				if (found == false) {// this is checking the reused value(170)
					System.out
							.println("addToDropDown(180): new Relationship being added");
					CachedRowSetImpl crsAID = query(String
							.format("Select ID from TBL_AIRCRAFT Where AIRCRAFT_IDENTIFIER = \"%s\"",
									AID));
					crsAID.next();
					CachedRowSetImpl crsRID = query(String
							.format("Select ID from TBL_RUNWAY Where RUNWAY_IDENTIFIER = \"%s\"",
									newData));
					crsRID.next();

					update(String
							.format("Insert Into TBL_EVENT (AIRCRAFT_ID, RUNWAY_ID) Values (%s,%s)",
									crsAID.getString("ID"),
									crsRID.getString("ID")));
					return getIDForData(field, newData);
				}
				System.out
						.println("addToDropDown: Relationship already in table");
				return getIDForData(field, newData);
			} else {
				System.out.println("addToDropDown: Field not valid");
				return 0;
			}
		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			return 0;
		}
	}

	public CachedRowSetImpl getAllFields() {
		CachedRowSetImpl Fields = query("Select FIELD from TBL_META");
		return Fields;
	}

	public int getIDForData(String field, String data) {
		try {
			CachedRowSetImpl metaData = query(String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK From TBL_META Where FIELD=\"%s\"",
							field));
			if (metaData.next()) {
				CachedRowSetImpl id = query(String.format(
						"Select ID From %s Where %s=\"%s\"",
						metaData.getString("INTERNAL_TABLE"),
						metaData.getString("INTERNAL_COLUMN"), data));
				if (id.next()) {
					return id.getInt("ID");
				}
			}
			return 0;
		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			System.out.println("getIDforData: Field or Data does not exist.");
			return 0;
		}
	}

	public String getDataForID(String field, int id) {
		try {
			CachedRowSetImpl metaData = query(String
					.format("Select INTERNAL_TABLE, INTERNAL_COLUMN, LINK From TBL_META Where FIELD=\"%s\"",
							field));
			if (metaData.next()) {
				CachedRowSetImpl data = query(String.format(
						"Select %s From %s Where ID=\"%s\"",
						metaData.getString("INTERNAL_COLUMN"),
						metaData.getString("INTERNAL_TABLE"), id));
				if (data.next()) {
					return data
							.getString(metaData.getString("INTERNAL_COLUMN"));
				} else {
					System.out
							.println("getDataForID: Relationship does not exist");
					return null;
				}
			} else {
				System.out.println("getDataForID: Field does not exist");
				return null;
			}

		} catch (SQLException ex) {
			Logger.getLogger(DataBaseAccess.class.getName()).log(Level.SEVERE,
					null, ex);
			// System.out.println("getIDforData: Field or Data does not exist.");
			return null;
		}
	}
}
