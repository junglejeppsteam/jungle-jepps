/////////////////////////////////////////////////////////////////////////
// Author: Jesse Sjoblom
// File: DataMerge.java
// Class: org.yajasi.JungleJepps.db.DataMerge
//
// Target Platform: Java Virtual Machine 
// Development Platform: Windows 8
// Development Environment: Netbeans 7.3.1
// 
// Project: Jungle Jepps - Desktop
// Copyright 2013 YAJASI. All rights reserved. 
//
/////////////////////////////////////////////////////////////////////////
package org.yajasi.JungleJepps.db;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.List;

import org.yajasi.JungleJepps.Field;
import org.yajasi.JungleJepps.Runway;
import org.yajasi.JungleJepps.db.DatabaseConnection;

/**
 * Builds the connection with any number of 3rd party read only db and a primary writable db
 * The class then manages them all.
 * @author jesse_000
 */
class DataMerge implements DatabaseConnection{
	//private DatabaseConnection primaryConnection;
        //private DatabaseConnection operationsConnection;
	//private DatabaseConnection primarySource;
        private DatabaseConnection[] connections;
	private SettingsManager settingsMgr;
        int numOfDbs;

	public DataMerge(SettingsManager settings)throws SQLException, DatabaseException{		
            
            // Keep reference to settings manager to retrieve overridden field list
            this.settingsMgr = settings;
            numOfDbs = settingsMgr.get(Settings.OPERATIONS_JDBC_CLASS_PATH).split("$").length+1;
            System.out.println("here: " + numOfDbs);
            connections = new DatabaseConnection[numOfDbs];
            // Keep reference to primary source to query when necessary
            this.connections[0] = new PrimaryJdbcSource(settings);
            for(int I = 1; I < numOfDbs; I++){
                this.connections[I] = new OperationsDatabase(settings, I-1); //the -1 is so that the OperationsDatabase does not have to account for the primary database 
            }
            
            //force primaryConnection to have all Keys from operationConnection
            ODPDclosure();
	}
	
        /**
         * Gets all Aircraft types form the DB
         * @return Aircraft types in an array;
         * @throws DatabaseException 
         */
	@Override
	public String[] getAllAircraftIds() throws DatabaseException {
            return connections[0].getAllAircraftIds();
	}
        
        /**
         * gets all runway IDs
         * @return runway IDs in an array
         * @throws DatabaseException 
         */
	@Override
	public String[] getAllRunwayIds() throws DatabaseException{
            return connections[0].getAllRunwayIds();
                
	}
        
	/**
         * gets runway IDs from the DB associated with an aircraft ID
         * @param aircraftId
         * @return runway IDs in an array
         * @throws DatabaseException 
         */
	@Override
	public String[] getAllRunwayIds(String aircraftId)throws DatabaseException {
            return connections[0].getAllRunwayIds(aircraftId);
	}
        
        /**
         * builds and returns a runway containing all read only data needed form the 3rd party db.
         * @param runwayId
         * @param aircraftId
         * @return
         * @throws DatabaseException 
         */
	@Override
	public Runway getRunway(String runwayId, String aircraftId)throws DatabaseException {
            //Runway operationsRunway = new Runway();
            RunwayBuilder results = connections[0].getTempRunway(runwayId, aircraftId);

            for(int I = numOfDbs-1; I > 0; I--){//this loop is in revers to make sure that the first 3rd party database has primacy on the results
                results.putAll(connections[I].getRunway(runwayId, aircraftId));
            }

            return Runway.initialize(results);
	}
        
        @Override
	public RunwayBuilder getTempRunway(String runwayId, String aircraftId)throws DatabaseException {
            //System.out.println("This method should never be used");
            throw new DatabaseException("getTempRunway method should never be used in DataMerge");
        }
        
        /**
         * updates the primary DB, but does nothing if you try to update the 3rd party db
         * @param runway
         * @return
         * @throws DatabaseException 
         */
	@Override
	public boolean updateRunway(Runway runway)throws DatabaseException {
            connections[0].updateRunway(runway);
            return true;
	}
	
	/**
	 * returns requested number of logs newest first
	 */
	@Override
	public String[][] getLog(String runwayId, String aircraftId, int requested) throws DatabaseException{
		
		return connections[0].getLog(runwayId, aircraftId, requested);
	}
	
        /**
         * Closes the DB connection. Use to avoided leaks
         * @return
         * @throws DatabaseException 
         */
	@Override
	public boolean close()throws DatabaseException {
		// TODO Auto-generated method stub
            for(int I = 0; I < numOfDbs; I++){
                connections[I].close();
            }
            return true;
	}
        
        /** This method is an example of how to query and work in a JDBC Context
         * Just a Test method. This method runs through each of the features of the class
         * in order to make sure everything is working.
         * @param args
         * @throws DatabaseException 
         */
        public static void main(String[] args) throws DatabaseException {//DBbuild test
            System.out.println("This is the main method of the datamerge Class\n");
            SettingsManager settings = SettingsManager.getInstance();
            //settings for mysql
            settings.setValue(Settings.PRIMARY_JDBC_CLASS_PATH, "com.mysql.jdbc.Driver"); 
            settings.setValue(Settings.PRIMARY_JDBC_URI, "jdbc:mysql://localhost/JJDB?user=jepps&password=jepps");
            settings.setValue(Settings.OPERATIONS_JDBC_CLASS_PATH, "com.mysql.jdbc.Driver");
            settings.setValue(Settings.OPERATIONS_JDBC_URI, "jdbc:mysql://localhost/JJDB3?user=jepps&password=jepps");
            settings.setOverrideColumn(Field.AIRCRAFT_IDENTIFIER, "craft");
            settings.setOverrideColumn(Field.RUNWAY_IDENTIFIER, "RUNWAY_ID");
            settings.setOverrideColumn(Field.PDF_PATH, "PDF");
            settings.setValue(Settings.OPERATIONS_TABLE_NAME, "a_id");
                       
            try{
                DataMerge db = new DataMerge(settings);
            
                String[] aircraftIds, runwayIds;
                Runway runway;

                ///getting runway and aircraft IDs
                aircraftIds = db.getAllAircraftIds();
                System.out.println("number of aircraft= " + aircraftIds.length);
                runwayIds = db.getAllRunwayIds( aircraftIds[0] );
                System.out.println("number of runway= " + runwayIds.length);
                runway = db.getRunway("KIW", "PC-6"); 

                System.out.print("\nAircraft IDs: ");
                for(String aid : aircraftIds)
                        System.out.print(aid + ", ");

                System.out.print("\nRunway IDs: ");
                for(String rid : runwayIds)
                        System.out.print(rid + ", ");


                System.out.print("\n\n-----" + "KIW" + "PC-6" + "---Field Printouts-----\n");
                for(Field f : runway.keySet())
                        System.out.println(f.toString() + ": " + runway.get(f) );


                db.close();
            }
            catch(SQLException e){
                System.out.println(e);
            }
        }
        
        private boolean ODPDclosure() throws DatabaseException{
            String[] operationsAID;
            String[] operationsRID;
            
            Runway operationsRunway = new Runway();
            Runway primaryRunway = new Runway();
            for(int I = 1; I < numOfDbs; I ++){
                operationsAID = connections[I].getAllAircraftIds();
                operationsRID = connections[I].getAllRunwayIds();
                for(String R: operationsRID){
                    for(String A: operationsAID){
                        operationsRunway = connections[I].getRunway(R, A);
                        if(!operationsRunway.isEmpty()){
                            primaryRunway.clear();
                            primaryRunway.put(Field.RUNWAY_IDENTIFIER, R);
                            primaryRunway.put(Field.AIRCRAFT_IDENTIFIER, A);
                            connections[0].updateRunway(primaryRunway);
                        }
                    }
                }
            }
            
            return false;
        }

		@Override
		public void publish(Runway runway) throws DatabaseException,
				SQLException {
			connections[0].publish(runway);
			// TODO Auto-generated method stub
			
		}

		@Override
		public String[][] getLog(String runwayId, String aircraftId,
				int requested, int type) throws DatabaseException {
			// TODO Auto-generated method stub
			return null;
		}
}
