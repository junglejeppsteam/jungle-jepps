/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yajasi.JungleJepps.db;

import java.util.ArrayList;
import java.util.HashMap;
import org.yajasi.JungleJepps.Field;
import org.yajasi.JungleJepps.Runway;
import org.yajasi.JungleJepps.ValueByEnum;

/**
 *
 * @author jesse_000
 */
public class RunwayBuilder extends HashMap<Field, String> implements ValueByEnum {
    
    public String get(Object key){
		Field field;
		if(key instanceof Field)
			field  = (Field) key;
		else if(key instanceof String)
			field = Field.valueOf( (String) key);
		else
			throw new ClassCastException("Key is not of type enum org.yajasi.JungleJepps.Field or java.lang.String");
		
		String val = super.get(field);
                return val == null ? "" : val; //Return empty string instead of null
	}
    
        public String put(Field field, String value){
		//return putOverride(field, value);
                return super.put(field, value);
	}
        
        public String get(JungleJeppsEnum key) {
		//Cast as object to avoid recursive method calls.
		return get( (Object) key ); 
	}
        
        public String[] toArray(){
        	ArrayList<String> list = new ArrayList<String>();
        	for(Field f: this.keySet()){
        		list.add(f + "#" + this.get(f));
        	}
        	return list.toArray(new String[list.size()]);
        }
        
        public static RunwayBuilder fromArray(String[] array){
        	RunwayBuilder runway = new RunwayBuilder();
        	String[] strValues;
        	for(String s: array){
        		strValues = s.split("#");
        		if(strValues.length < 2){
        			continue;
        		}
        			runway.put(Field.valueOf(strValues[0].toUpperCase()), strValues[1]);
        	}
        	return runway;
        }
        
        public static RunwayBuilder fromRunway(Runway inRunway){
        	RunwayBuilder runway = new RunwayBuilder();
        	for(Field f: inRunway.keySet()){
        		runway.put(f,inRunway.get(f));
        	}
        	return runway;
        }
        
        public static void main(String[] args){
        	RunwayBuilder runwayA = new RunwayBuilder();
        	RunwayBuilder runwayB = new RunwayBuilder();
        	runwayA.put(Field.RUNWAY_IDENTIFIER, "KIW");
			runwayA.put(Field.AIRCRAFT_IDENTIFIER, "PC-6");
			runwayA.put(Field.LATITUDE, "tempLat");
			runwayA.put(Field.LONGITUDE, "tempLon");
			System.out.println("RunwayA");
			for(Field f: runwayA.keySet()){
				System.out.println(f+": "+runwayA.get(f));
			}
			
			runwayB = RunwayBuilder.fromArray(runwayA.toArray());
			System.out.println("RunwayB");
			for(Field f: runwayB.keySet()){
				System.out.println(f+": "+runwayB.get(f));
			}
        }
}
