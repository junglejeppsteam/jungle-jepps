package org.yajasi.JungleJepps.testing.ui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class FivePanel extends JFrame {

	public static void main(String[] args) {
		// make new FivePanel and set visible

		FivePanel fp = new FivePanel();
		fp.setVisible(true);
	}

	public FivePanel() {
		//declarations
		
		GridBagLayout gbl;
		JPanel p[];
		int c = 0x00FFFF;
		int grid = 0;
		
		
		//TODO set icon
		
		// setup frame
		// TODO make panel start at less than screen resolution
		setBounds(100, 100, 500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		// make layout and set
        gbl = new GridBagLayout();
		setLayout(gbl);
		
		p = new JPanel[5];
		for(JPanel jp : p){
			jp = new JPanel();
			GridBagConstraints g = new GridBagConstraints();
			g.gridy = grid++;
			add(jp);
			jp.setBackground(new Color(c += 0xFF0000 / 5));
			System.out.println(jp.getBackground().toString());
		}


	}
}
