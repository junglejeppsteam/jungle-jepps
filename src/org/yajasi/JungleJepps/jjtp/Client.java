package org.yajasi.JungleJepps.jjtp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.yajasi.JungleJepps.Field;
import org.yajasi.JungleJepps.Runway;
import org.yajasi.JungleJepps.db.DatabaseConnection;
import org.yajasi.JungleJepps.db.DatabaseException;
import org.yajasi.JungleJepps.db.RunwayBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;



public class Client implements DatabaseConnection {
	private HttpClient client; 
	//private Class<T> type;
	
	/**
	 * This example demonstrates the retrieval of runway ids from 
	 * a LAN conected server.
	 * @param args
	 * @throws InterruptedException
	 * @throws DatabaseException 
	 */
	public static void main(String[] args) throws InterruptedException, DatabaseException{
		Client client = new Client();
		Thread.sleep(4000);
		
		String[][] log = client.getLog("KIW", "PC-6", 5);
		System.out.println("---Log---");
		for(String[] A: log){
			for(String S: A){
				System.out.print(S +" , ");
			}
			System.out.print("\n");
		}
		System.out.println("-End Log-");
		
		//Runway runway = client.getRunway("KIW", "PC-6");
		//System.out.println(runway.get(Field.IMAGE_PATH));
		
		//String runwayId[] = client.getAllRunwayIds("");
		//System.out.println("Runways---");
		//for(String s: runwayId){
		//	System.out.println(s);
		//}
		//System.out.println("Runways---");
		
		//String aircraftId[] = client.getAllAircraftIds();
		//String runwayId[] = client.getAllRunwayIds(aircraftId[0]);		
		//System.out.println(Field.AIRCRAFT_IDENTIFIER.toString()+": "+ aircraftId[0]);
		//System.out.println(Field.RUNWAY_IDENTIFIER.toString()+": "+ runwayId[0]);
		//String[] runways = client.getAllRunwayIds(aircraftId);
		//for(String runway : runwayId){
		//	System.out.println(runway);
		//}
		//Runway runway = client.getRunway(runwayId[0],aircraftId[0]);
		//System.out.println("---Runway Print Out Start---");
		//for(Field f: runway.keySet()){
		//	System.out.println(f.toString() + ":  " + runway.get(f));
		//}
		//System.out.println("----Runway Print Out End----");
	}
	
	
	/**
	 * Constructor to create a new JungleJepps DatabaseConnection 
	 * to access the database on a primary instance over the LAN
	 */
	public Client() {
		client = HttpClientBuilder.create().build();
		//type = t;
		JungleJeppsmDNS.startDiscovery();
	} 
	
	/////////////////////////////////////////////////////////////////////////////////
	// START DatabaseConnection Implementation
	//
	/////////////////////////////////////////////////////////////////////////////////

	@Override
	public String[] getAllAircraftIds() throws DatabaseException{
		String[] list; // To store all the Ids
		Gson gson = new Gson(); // Google's json parser
		BufferedReader reader; // Reader from server json message

		reader = getApplicationJson("/application/aircraft/", null);
		
		// Build the response data back into a String[] 
		list = gson.fromJson(reader, String[].class);	
		return list;	}
	
	public String[] getAllRunwayIds() throws DatabaseException {
		return getAllRunwayIds("");
	}
	
	/**
	 * This method will return all the runway Ids for this database connection
	 */
	@Override
	public String[] getAllRunwayIds(String aircraftId) throws DatabaseException {
		String[] list; // To store all the Ids
		Gson gson = new Gson(); // Google's json parser
		BufferedReader reader; // Reader from server json message
		
		if(aircraftId.length() > 0){
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(new BasicNameValuePair("aid", aircraftId));
		
			reader = getApplicationJson("/application/runway/", parameters);
		}
		else{
			reader = getApplicationJson("/application/runway/", null);
		}
		
		
		// Build the response data back into a String[] 
		list = gson.fromJson(reader, String[].class);	
		return list;
	}

	@Override
	public Runway getRunway(String runwayId, String aircraftId) throws DatabaseException {
		Runway runway; //will be build out of the temprunway
		RunwayBuilder tempRunway; //used to build runway
		String[] strRunway;//The temprunway object to be deserialized into
		Gson gson = new Gson(); // Google's json parser
		BufferedReader reader; // Reader from server json message
		boolean cont;
		String tempStr;
		
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("rid", runwayId));
		parameters.add(new BasicNameValuePair("aid", aircraftId));
		
		reader = getApplicationJson("/application/runway/", parameters);
		
		try {
			while(true){
				tempStr = reader.readLine();
				if(tempStr == null){
					break;
				}
				System.out.println(tempStr);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		reader = getApplicationJson("/application/runway/", parameters);
		// Build the response data back into a Runway 
		//Type type = new TypeToken<HashMap<Field, String>>(){}.getType();
		try {
			strRunway = gson.fromJson(reader, String[].class);
			tempRunway = RunwayBuilder.fromArray(strRunway);
			return Runway.initialize(tempRunway);
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}

	@Override
	public boolean updateRunway(Runway runway) throws DatabaseException {
		StatusLine status;
		Gson gson = new Gson(); // Google's json parser
		RunwayBuilder tempRunway = RunwayBuilder.fromRunway(runway);
		String[] strArray = tempRunway.toArray();
		String json = gson.toJson(strArray);
		
		status = sendApplicationJson("/application/runway/update", null, json);
		
		if(status.getStatusCode() != HttpStatus.SC_OK)
			throw new DatabaseException(status.getReasonPhrase());
		
		return true;
	}
	
	public InputStream getSettingsStream(){
		URI uri = null; //The URI that will be built
		HttpResponse response = null; // The response from the Server
		InputStream is = null; // What will read the response from the server 
		HttpGet request = new HttpGet(); // Using HTTP method GET
		
		// Application requests for settings sends it as plain text to load directly into the settings manager
		request.addHeader("Accept", "text/plain");
		
		try {
			uri = new URIBuilder( JungleJeppsmDNS.getProvider() ) // Partial URI containing host and port <host>:<port> 
						.setScheme("http") // Not secure
						.setPath("/settings") // Request to list all runway ids
						.build();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
					
		request.setURI(uri); // Use this URI for the request
		
		try {
			response = client.execute(request); // Execute HTTP method 
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			// Create reader for the response data
			is = response.getEntity().getContent();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Return the raw InputStream 
		return is;
	}


	@Override
	public boolean close() {
		JungleJeppsmDNS.stopDiscovery();
		client = null;
		return true;
	}

	
	private BufferedReader getApplicationJson(String path, List<NameValuePair> parameters) throws DatabaseException{		
		URI uri = null; //The URI that will be built
		HttpResponse response = null; // The response from the Server
		BufferedReader reader = null; // What will read the response from the server 
		HttpGet request = new HttpGet(); // Using HTTP method GET
		
		// Application requests over LAN will only use JSON
		request.addHeader("Accept", "application/json");
		
		try {
			if(parameters == null){
				uri = new URIBuilder( JungleJeppsmDNS.getProvider() ) // Partial URI containing host and port <host>:<port> 
						.setScheme("http") // Not secure
						.setPath(path) // Request path
						.build();
			}
			else{
				uri = new URIBuilder( JungleJeppsmDNS.getProvider() ) // Partial URI containing host and port <host>:<port> 
						.setScheme("http") // Not secure
						.setPath(path) // Request path
						.addParameters(parameters) //Add url parameters
						.build();
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		}

		request.setURI(uri); // Use this URI for the request
		
		try {
			response = client.execute(request); // Execute HTTP method 
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		} catch (IOException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		}
		
		try {
			// Create reader for the response data
			reader = new BufferedReader( new InputStreamReader(response.getEntity().getContent()));
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		} catch (IOException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		}
		
		return reader;
	}
	
	private StatusLine sendApplicationJson(String path, List<NameValuePair> parameters, String body) throws DatabaseException{
		URI uri = null; //The URI that will be built
		HttpResponse response = null; // The response from the Server
		BufferedReader reader = null; // What will read the response from the server 
		HttpPatch request = new HttpPatch(); // Using HTTP method PATCH
		
		// Application requests over LAN will only use JSON
		request.addHeader("Accept", "application/json");
		
		try {
			if(parameters != null){
				uri = new URIBuilder( JungleJeppsmDNS.getProvider() ) // Partial URI containing host and port <host>:<port> 
						.setScheme("http") // Not secure
						.setPath(path) // Request path
						.addParameters(parameters) //Add url parameters
						.build();
			}
			else{
				uri = new URIBuilder( JungleJeppsmDNS.getProvider() ) // Partial URI containing host and port <host>:<port> 
						.setScheme("http") // Not secure
						.setPath(path) // Request path
						.build();
			}
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		}

		request.setURI(uri); // Use this URI for the request
		
		try {
			request.setEntity( new StringEntity(body) ); //Set the body of the request
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		}
		
		try {
			response = client.execute(request); // Execute HTTP method 
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new DatabaseException(e);			
		} catch (IOException e) {
			e.printStackTrace();
			throw new DatabaseException(e);
		}
		
		// Return its status
		return response.getStatusLine();
	}


	@Override
	public RunwayBuilder getTempRunway(String runwayId, String aircraftId)
		//method is not needed for client as the server side will do this.	
			throws DatabaseException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String[][] getLog(String runwayId, String aircraftId,int requested)throws DatabaseException { 
		return getLog(runwayId, aircraftId, requested, -1);
	}


	@Override
	public void publish(Runway runway) throws DatabaseException, SQLException {
		// TODO Auto-generated method stub	
	}


	@Override
	public String[][] getLog(String runwayId, String aircraftId,
			int requested, int type) throws DatabaseException {
		String[][] log; //The logs object to be deserialized into
		Gson gson = new Gson(); // Google's json parser
		BufferedReader reader; // Reader from server json message
		
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("rid", runwayId));
		parameters.add(new BasicNameValuePair("aid", aircraftId));
		parameters.add(new BasicNameValuePair("requested", Integer.toString(requested)));
		parameters.add(new BasicNameValuePair("type", Integer.toString(type)));
		
		reader = getApplicationJson("/application/log/", parameters);
		
		// Build the response data back into a Runway 
		log = gson.fromJson(reader, String[][].class);	
		return log;	
	}


	
}