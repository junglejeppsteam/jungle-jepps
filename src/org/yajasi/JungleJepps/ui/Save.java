package org.yajasi.JungleJepps.ui;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.text.JTextComponent;

import org.yajasi.JungleJepps.Field;
import org.yajasi.JungleJepps.Runway;
import org.yajasi.JungleJepps.db.DataBaseAccess;

/**
 * This class is used to set a field element to save when changed. Ther user
 * interface makes frequent calls to initiate the fields to listen for changes
 * and then save.
 * 
 * @author Joel Jeske
 */
public class Save {

	private Save() {
	}

	// Holds the current/open runway for easy saving
	protected static String runway;
	protected static String aircraft;
	protected static DataBaseAccess dba;

	// Dependency injector for runtime saving
	public static void setRunway(String runway) {
		Save.runway = runway;
	}

	public static void setAircraft(String aircraft) {
		Save.aircraft = aircraft;
	}

	public static void setDBA(DataBaseAccess d) {
		dba = d;
	}

	public static void onChange(JComboBox<Object> element, Field Field) {
		// Create new listener and inject its field to save immediately
		new Save.SaveOnChange(element).fieldToSave = Field;
	}

	public static void onChange(JTextComponent element, Field Field) {
		// Create new listener and inject its field to save immediately
		new Save.SaveOnChange(element).fieldToSave = Field;
	}

	public static void onChange(JCheckBox element, Field Field) {
		// Create new listener and inject its field to save immediately
		new Save.SaveOnChange(element).fieldToSave = Field;
	}

	protected static class SaveOnChange extends OnChange {
		protected Field fieldToSave;

		public SaveOnChange(JComboBox<Object> field) {
			super(field);
		}

		public SaveOnChange(JTextComponent field) {
			super(field);
		}

		public SaveOnChange(JCheckBox field) {
			super(field);
		}

		@Override
		public void onChange(String value) {
			// Save to runtime object always
			String f = fieldToSave.toString().toUpperCase();

			boolean r = dba.setField(f, aircraft, runway, value);
			if (!r)
				System.err.println("Could not set field: "
						+ f + " data: "
						+ value + " aircraft: " + aircraft + " runway: " + runway);
		}

	}

}
