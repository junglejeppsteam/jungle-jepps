package org.yajasi.JungleJepps.ui;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;

import org.yajasi.JungleJepps.Field;
import org.yajasi.JungleJepps.Runway;
import org.yajasi.JungleJepps.db.DataBaseAccess;
import org.yajasi.JungleJepps.db.DatabaseConnection;
import org.yajasi.JungleJepps.db.DatabaseException;
import org.yajasi.JungleJepps.db.DatabaseManager;
import org.yajasi.JungleJepps.db.RunwayBuilder;
import org.yajasi.JungleJepps.db.Settings;
import org.yajasi.JungleJepps.db.SettingsManager;
import org.yajasi.JungleJepps.pdf.Repository;

import com.toedter.calendar.JCalendar;

@SuppressWarnings("serial")
public class NewUI extends JFrame {
	private final SettingsManager settings;
	private final DatabaseConnection db;
	private Runway runway;

	JLabel lblImage;

	HashMap<JLabel, Field> labels = new HashMap<JLabel, Field>();
	HashMap<JLabel, Settings> labelunits = new HashMap<JLabel, Settings>();
	HashMap<JTextComponent, Field> textComponents = new HashMap<JTextComponent, Field>();
	HashMap<JCheckBox, Field> checkBoxes = new HashMap<JCheckBox, Field>();
	HashMap<JComboBox<Object>, Field> comboBoxes = new HashMap<>();

	HashMap<Field, FieldComponent> fcomponents;

	private JTextField txtRepositoryPath;
	private JTextField txtPrimaryJdbcUri;
	private JTextField txtPrimaryJdbcClassPath;
	private JTextField txtDistanceConvertFactor;
	private JTextField txtAdminEmail;
	private JTextField txtAdminPassword;
	private JTextField txtMagneticVariation;
	private JTextField txtDefaultExpirationPeriod;

	private JTextArea txtChangeLog;

	private DataTab dataTab;

	private ComponentMap cmap;

	DataBaseAccess dataBase;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {

				NewUI frame = new NewUI();
				frame.setVisible(true);
			}
		});
	}

	private void formWindowClosing(java.awt.event.WindowEvent evt) {
		try {
			db.close();
			settings.save();
		} catch (DatabaseException ex) {
			showError(ex);
		}
	}

	private void loadRunway() {
		String runwayId = getRunwayId();
		String aircraftId = getAircraftId();
		String[][] log;

		try {
			runway = db.getRunway(runwayId, aircraftId);
			//Save.setRunway(runway);
			log = db.getLog(runwayId, aircraftId, 10, -1);
			if (log != null && log.length > 0 && txtChangeLog != null) {
				txtChangeLog.setText("");
				for (int I = 0; I < log.length; I++) {
					txtChangeLog.append("\n" + log[I][8] + ": " + log[I][6]);
					System.out.println(log[I][6]);
				}
			}

		} catch (DatabaseException e) {
			e.printStackTrace();
			showError(e);
		}
	}

	private void showError(Exception e) {
		e.printStackTrace();
	}

	private String[] getAircraftIds() {
		String[] ids;
		try {
			ResultSet rs = dataBase.getDropDown(Field.AIRCRAFT_IDENTIFIER
					.toString().toUpperCase());
			ArrayList<String> al = new ArrayList<>();

			while (rs.next()) {
				al.add(rs.getString(1));
			}

			ids = al.toArray(new String[al.size()]);
		} catch (SQLException e) {
			showError(e);
			return null;
		}

		return ids;
	}

	// Binds a component to a Field
	private abstract class FieldComponent {
		Field componentField;
		JLabel label;

		public FieldComponent(Field f) {
			componentField = f;
			label = new JLabel();
			loadLabel();
		}

		public abstract JComponent getComponent();

		public abstract void load();

		public JLabel getLabel() {
			return label;
		}

		public void loadLabel() {
			label.setText(settings.getLabel(componentField));
		}
	}

	// Bind a field to a check box
	private class FieldCheckBox extends FieldComponent {
		JCheckBox cb;

		public FieldCheckBox(Field f) {
			super(f);
			cb = new JCheckBox();
			Save.onChange(cb, super.componentField);
			load();
		}

		@Override
		public JComponent getComponent() {
			return cb;
		}

		@Override
		public void load() {
			cb.setSelected(Boolean.parseBoolean(getField(super.componentField)));
		}
	}

	// Bind a field to a text field
	private class FieldTextField extends FieldComponent {
		JTextField cb;

		public FieldTextField(Field f) {
			super(f);
			cb = new JTextField();
			Save.onChange(cb, super.componentField);
			load();
		}

		@Override
		public JComponent getComponent() {
			return cb;
		}

		@Override
		public void load() {
			cb.setText(getField(super.componentField));
		}
	}

	// Bind the run way combo box to its field
	private class RunwayComboBox extends FieldComponent implements
			FieldComboBox {
		JComboBox<Object> jcb;

		public RunwayComboBox(Field f) {
			super(f);
			super.label.setFont(new Font("Tahoma", Font.BOLD, 15));
			jcb = new JComboBox<>();
			load();
			new OnChange(jcb) {
				public void onChange(String value) {
					settings.setValue(Settings.LAST_RUNWAY, value);
					Save.setRunway(value);
					cmap.loadAll();
					dataTab.resetScrollBar();
					dataTab.refreshTitle();
				}
			};
		}

		@Override
		public JComponent getComponent() {
			return jcb;
		}

		@Override
		public void load() {
			jcb.setModel(new JJRunwayCBM());
			jcb.setSelectedItem(settings.get(Settings.LAST_RUNWAY));
		}

		@Override
		public JComboBox<Object> getComboBox() {
			return jcb;
		}
	}

	private class AircraftComboBox extends FieldComponent implements
			FieldComboBox {
		JComboBox<Object> jcb;
		String[] aircraftIds;

		public AircraftComboBox(Field f) {
			super(f);
			super.label.setFont(new Font("Tahoma", Font.BOLD, 15));
			jcb = new JComboBox<>();
			load();
			new OnChange(jcb) {
				public void onChange(String value) {
					settings.setValue(Settings.LAST_AIRCRAFT, value);
					cmap.get(Field.RUNWAY_IDENTIFIER).load();
					Save.setAircraft(value);
					Save.setRunway(getRunwayId());
					cmap.loadAll();
					
				}
			};
		}

		@Override
		public JComponent getComponent() {
			return jcb;
		}

		@Override
		public void load() {
			jcb.setModel(new JJAircraftCBM(getAircraftIds()));
			jcb.setSelectedItem(settings.get(Settings.LAST_AIRCRAFT));
		}

		@Override
		public JComboBox<Object> getComboBox() {
			return jcb;
		}

	}

	private class JJComboBox extends FieldComponent implements FieldComboBox {
		JComboBox<Object> jcb;

		public JJComboBox(Field f) {
			super(f);
			jcb = new JComboBox<>();
			Save.onChange(jcb, super.componentField);
			load();
		}

		@Override
		public JComponent getComponent() {
			return jcb;
		}

		@Override
		public void load() {
			ResultSet r = dataBase.getDropDown(super.componentField.toString()
					.toUpperCase());
			ArrayList<String> l = new ArrayList<>();
			try {
				while (r.next()) {
					String s = r.getString(1);
					l.add(s != null ? s : "ERROR!");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			jcb.setModel(new JJCBM(l.toArray(new String[l.size()]),
					super.componentField));
			jcb.setSelectedItem(getField(super.componentField));
		}

		@Override
		public JComboBox<Object> getComboBox() {
			return jcb;
		}

	}

	private class ComponentMap {
		Map<Field, FieldComponent> components;
		Map<Field, FieldComboBox> comboBoxes;
		Map<Settings, SettingsComponent> settingsComponents;

		HashSet<Field> fieldCheckBox;
		HashSet<Field> fieldTextField;
		HashSet<Field> settingsComboBox;
		HashSet<Field> fieldTextArea;
		HashSet<Field> comboBox;

		HashSet<Settings> settingsCheckBox;
		HashSet<Settings> settingsTextField;
		HashSet<Settings> settingsTextArea;
		HashSet<Settings> settingsDistance;

		public ComponentMap() {
			components = new HashMap<>();
			comboBoxes = new HashMap<>();
			settingsComponents = new HashMap<>();

			fillSets();
		}

		private void fillSets() {
			fieldCheckBox = new HashSet<>();
			fieldCheckBox.add(Field.INSPECTION_NA);
			fieldCheckBox.add(Field.PRECIPITATION_ON_SCREEN_HL);
			fieldCheckBox.add(Field.ELEVATION_HL);
			fieldCheckBox.add(Field.LENGTH_HL);
			fieldCheckBox.add(Field.WIDTH_TEXT_HL);
			fieldCheckBox.add(Field.TDZ_SLOPE_HL);
			fieldCheckBox.add(Field.IAS_ADJUSTMENT_HL);
			fieldCheckBox.add(Field.RUNWAY_A_HL);
			fieldCheckBox.add(Field.A_TAKEOFF_RESTRICTION_HL);
			fieldCheckBox.add(Field.A_TAKEOFF_NOTE_HL);
			fieldCheckBox.add(Field.A_LANDING_RESTRICTION_HL);
			fieldCheckBox.add(Field.A_LANDING_NOTE_HL);
			fieldCheckBox.add(Field.RUNWAY_B_HL);
			fieldCheckBox.add(Field.B_TAKEOFF_RESTRICTION_HL);
			fieldCheckBox.add(Field.B_TAKEOFF_NOTE_HL);
			fieldCheckBox.add(Field.B_LANDING_RESTRICTION_HL);
			fieldCheckBox.add(Field.B_LANDING_NOTE_HL);

			fieldTextField = new HashSet<>();
			fieldTextField.add(Field.INSPECTION_DUE);
			fieldTextField.add(Field.INSPECTION_DUE);
			fieldTextField.add(Field.INSPECTION_DATE);
			fieldTextField.add(Field.LANGUAGE_GREET);
			fieldTextField.add(Field.ELEVATION);
			fieldTextField.add(Field.LENGTH);
			fieldTextField.add(Field.WIDTH_TEXT);
			fieldTextField.add(Field.TDZ_SLOPE);
			fieldTextField.add(Field.IAS_ADJUSTMENT);
			fieldTextField.add(Field.LONGITUDE);
			fieldTextField.add(Field.LATITUDE);
			fieldTextField.add(Field.RUNWAY_A);
			fieldTextField.add(Field.A_TAKEOFF_RESTRICTION);
			fieldTextField.add(Field.A_LANDING_RESTRICTION);
			fieldTextField.add(Field.RUNWAY_B);
			fieldTextField.add(Field.B_TAKEOFF_RESTRICTION);
			fieldTextField.add(Field.B_LANDING_RESTRICTION);

			settingsComboBox = new HashSet<>();
			settingsComboBox.add(Field.INSPECTOR_NAME);
			settingsComboBox.add(Field.CLASSIFICATION);
			settingsComboBox.add(Field.FREQUENCY_1);
			settingsComboBox.add(Field.FREQUENCY_2);
			settingsComboBox.add(Field.PRECIPITATION_ON_SCREEN);

			fieldTextArea = new HashSet<>();
			fieldTextArea.add(Field.A_TAKEOFF_NOTE);
			fieldTextArea.add(Field.A_LANDING_NOTE);
			fieldTextArea.add(Field.B_TAKEOFF_NOTE);
			fieldTextArea.add(Field.B_LANDING_NOTE);

			comboBox = new HashSet<>();
			comboBox.addAll(settingsComboBox);
			comboBox.add(Field.RUNWAY_IDENTIFIER);
			comboBox.add(Field.AIRCRAFT_IDENTIFIER);

			settingsCheckBox = new HashSet<>();
			settingsCheckBox.add(Settings.IS_PRIMARY);
			settingsCheckBox.add(Settings.IS_TRUE_COURSE);

			settingsTextField = new HashSet<>();
			settingsTextField.add(Settings.PRIMARY_JDBC_URI);
			settingsTextField.add(Settings.PRIMARY_JDBC_CLASS_PATH);
			settingsTextField.add(Settings.DISTANCE_CONVERT_FACTOR);
			settingsTextField.add(Settings.ADMIN_EMAIL);
			settingsTextField.add(Settings.ADMIN_PASSWORD);
			settingsTextField.add(Settings.MAGNETIC_VARIATION);
			settingsTextField.add(Settings.DEFAULT_EXPIRATION_PERIOD);

			settingsTextArea = new HashSet<>();
			settingsTextArea.add(Settings.PAGE_1_DISCLAIMER);
			settingsTextArea.add(Settings.PAGE_2_DISCLAIMER);

			settingsDistance = new HashSet<>();
			settingsDistance.add(Settings.ALTITUDE_UNITS);
			settingsDistance.add(Settings.DISTANCE_UNITS);
			settingsDistance.add(Settings.DIMENSION_UNITS);
		}

		public JComponent getComponent(Field f) {
			return get(f).getComponent();
		}

		public FieldComponent get(Field f) {
			FieldComponent fc = components.get(f);

			if (fc == null) {
				initComponent(f);
				fc = components.get(f);
			}

			return fc;
		}

		public SettingsComponent get(Settings s) {
			SettingsComponent sc = settingsComponents.get(s);

			if (sc == null) {
				initComponent(s);
				sc = get(s);
			}

			return sc;
		}

		public void loadAll() {
			for (FieldComponent fc : components.values()) {
				fc.load();
				fc.loadLabel();
			}

		}

		public JComboBox<Object> getComboBox(Field f) {
			if (get(f) == null)
				initComponent(f);
			FieldComboBox fcb = comboBoxes.get(f);
			return fcb == null ? null : fcb.getComboBox();
		}

		private FieldComponent createComponent(Field f) {
			if (fieldCheckBox.contains(f))
				return new FieldCheckBox(f);
			else if (fieldTextField.contains(f))
				return new FieldTextField(f);
			else if (settingsComboBox.contains(f))
				return new JJComboBox(f);
			else if (fieldTextArea.contains(f))
				return new FieldTextArea(f);
			else if (f == Field.RUNWAY_IDENTIFIER)
				return new RunwayComboBox(f);
			else if (f == Field.AIRCRAFT_IDENTIFIER)
				return new AircraftComboBox(f);
			else
				return null;
		}

		private SettingsComponent createComponent(Settings s) {
			if (settingsCheckBox.contains(s))
				return new SettingsCheckBox(s);
			else if (s == Settings.REPOSITORY_PATH)
				return new settingsFileChooser(s);
			else if (settingsTextField.contains(s))
				return new SettingsTextField(s);
			else if (settingsDistance.contains(s))
				return new SettingsDistance(s);
			else if (s == Settings.WEIGHT_UNITS)
				return new SettingsWeight(s);
			else if (s == Settings.LONG_LAT_FORMAT)
				return new SettingsCoordinateFormat(s);
			else
				return null;
		}

		private void initComponent(Field f) {

			FieldComponent fc = createComponent(f);
			components.put(f, fc);

			if (comboBox.contains(f))
				comboBoxes.put(f, (FieldComboBox) fc);
		}

		private void initComponent(Settings s) {
			SettingsComponent sc = createComponent(s);
			settingsComponents.put(s, sc);
		}
	}

	private interface FieldComboBox {
		public JComboBox<Object> getComboBox();
	}

	private class JJComboBoxModel extends AbstractListModel<Object> implements
			ComboBoxModel<Object> {
		private Object item;
		private String[] options;

		public JJComboBoxModel(String[] opts) {
			options = opts;
		}

		public JJComboBoxModel() {

		}

		@Override
		public void setSelectedItem(Object anItem) {
			item = anItem;
		}

		@Override
		public Object getSelectedItem() {
			return item;
		}

		@Override
		public int getSize() {
			return options.length;
		}

		@Override
		public String getElementAt(int index) {
			return options[index];
		}
	}

	private abstract class JJDynamicCBM extends JJComboBoxModel {

		private JDialog jd;

		public JJDynamicCBM(String[] opts) {
			super(opts);
			addEditOption();
		}

		public abstract String getEditOption();

		// add menu option to edit options
		public void addEditOption() {
			String[] options = new String[this.getSize() + 1];

			System.arraycopy(super.options, 0, options, 0, super.options.length);
			options[options.length - 1] = getEditOption();
			super.options = options;
		}

		// add an option to the combo box
		public abstract void add(String item);

		// delete an option from the combo box
		public abstract void delete(String item);

		// show the dialog
		public void showDialog() {
			jd = new JDialog(NewUI.this);
			final JTextField txtAdd = new JTextField();
			JButton hide = new JButton("hide");
			JButton add = new JButton("add");

			txtAdd.setColumns(10);

			jd.setLayout(new FlowLayout());

			jd.add(hide);
			jd.add(add);
			jd.add(txtAdd);
			jd.pack();

			hide.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					hideDialog();

				}
			});

			add.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {

					add(txtAdd.getText());
					hideDialog();
				}
			});

			jd.setLocationRelativeTo(NewUI.this);
			jd.setVisible(true);
		}

		// hide the dialog
		public void hideDialog() {
			jd.dispose();
		}

		// show dialog if option is selected
		@Override
		public void setSelectedItem(Object item) {
			if (item.equals(getEditOption()))
				showDialog();
			else
				super.setSelectedItem(item);
		}
	}

	private class JJRunwayCBM extends JJDynamicCBM implements
			ComboBoxModel<Object> {

		public JJRunwayCBM() {

			super(getRunwayIds(getAircraftId()));
		}

		@Override
		public void add(String item) {
			int status = dataBase.addToDropDown(Field.RUNWAY_IDENTIFIER
					.toString().toUpperCase(), getAircraftId(), item);

			if (status != 0) {
				JOptionPane.showMessageDialog(super.jd, "Success");
				cmap.get(Field.RUNWAY_IDENTIFIER).load();
			} else
				JOptionPane.showMessageDialog(super.jd, "Failed");
		}

		@Override
		public void delete(String item) {

			JOptionPane.showMessageDialog(super.jd, "deleted: " + item);

		}

		@Override
		public String getEditOption() {
			return "Edit Runways";
		}

	}

	private class JJAircraftCBM extends JJDynamicCBM implements
			ComboBoxModel<Object> {

		public JJAircraftCBM(String[] opts) {
			super(opts);
		}

		@Override
		public String getEditOption() {
			return "Edit Aircraft";
		}

		@Override
		public void add(String item) {
			int status = dataBase.addToDropDown(Field.AIRCRAFT_IDENTIFIER
					.toString().toUpperCase(), item);

			if (status != 0) {
				JOptionPane.showMessageDialog(super.jd, "Success");
				cmap.get(Field.AIRCRAFT_IDENTIFIER).load();
			} else
				JOptionPane.showMessageDialog(super.jd, "Failed");
		}

		@Override
		public void delete(String item) {
			// not implemented

		}

	}

	private class JJCBM extends JJDynamicCBM {
		private Field f;

		public JJCBM(String[] opts, Field f) {
			super(opts);
			this.f = f;
		}

		@Override
		public String getEditOption() {
			return "Edit Options";
		}

		@Override
		public void add(String item) {
			int status = dataBase.addToDropDown(f.toString().toUpperCase(),
					item);

			if (status != 0) {
				JOptionPane.showMessageDialog(super.jd, "Success");
				cmap.get(f).load();
			} else
				JOptionPane.showMessageDialog(super.jd, "Failed");

		}

		@Override
		public void delete(String item) {
			// Not yet implemented

		}

	}

	private class JJDefaultsComboBoxModel extends JJComboBoxModel implements
			ComboBoxModel<Object> {
		private Field field;
		private final static String defaultChangeOption = "--Change Defaults--";
		private JComboBox<Object> comboBox;

		public JJDefaultsComboBoxModel(JComboBox<Object> comboBox, Field field) {
			// Construct upper model
			super();

			this.comboBox = comboBox;

			// Get current defaults
			String[] defaults = settings.getDefaults(field);

			// The the defaults into the super.options String[]
			loadDefaults(defaults);

			// Keep a handle on the field to load the
			this.field = field;
		}

		// Helper function
		private void loadDefaults(String[] defs) {
			// Construct new array with an extra spot at the bottom
			super.options = new String[defs.length + 1];

			for (int i = 0; i < defs.length; i++) {
				super.options[i] = defs[i];
			}

			// Add default item to bottom of list
			super.options[super.options.length - 1] = defaultChangeOption;
		}

		@Override
		public void setSelectedItem(Object anItem) {
			if (defaultChangeOption.equals(anItem)) {
				StringBuilder initializer = new StringBuilder();
				// Loop through every option except the one to change defaults
				for (int i = 0; i < super.options.length - 1; i++) {
					if (!super.options[i].isEmpty()) // If there is a
														// default
					{
						initializer.append(super.options[i]);
						initializer.append("\n");
					}
				}

				final JDialog popup = new JDialog();
				popup.setTitle("Choose menu defaults for <"
						+ settings.getLabel(field) + ">");
				popup.setSize(400, 300);

				javax.swing.JPanel innerPanel = new javax.swing.JPanel(); // will
																			// hold
																			// label
																			// and
																			// text
																			// area

				// Graphical Layout:
				// <Label> <text area>
				// <Cancel> <Save>
				innerPanel.setLayout(new GridLayout(2, 2));

				final javax.swing.JTextArea defaults = new javax.swing.JTextArea();
				javax.swing.JButton cancel = new javax.swing.JButton("Cancel");
				javax.swing.JButton save = new javax.swing.JButton("Save");

				cancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// Discard window
						popup.dispose();
					}
				});

				save.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// Get and split resultant into String[]
						String result = defaults.getText();
						String[] newDefaults = result.trim().split("\n");

						// Save defaults in settings
						settings.setDefaults(field, newDefaults);

						// Save the new settings in this runtime
						JJDefaultsComboBoxModel model = new JJDefaultsComboBoxModel(
								comboBox, field);
						model.setSelectedItem(getSelectedItem());
						comboBox.setModel(model);

						// Discard window
						popup.dispose();
					}
				});

				defaults.setText(initializer.toString());
				innerPanel.add(new JLabel("Menu Defaults (on separate lines)"));
				innerPanel.add(defaults);
				innerPanel.add(cancel);
				innerPanel.add(save);

				popup.getContentPane().add(innerPanel);
				popup.setVisible(true);

			} else // if not the default option
			{
				// Set the model box to this item
				super.setSelectedItem(anItem);
			}
		}

	}

	private String[] getRunwayIds(String aircraftId) {
		String[] ids;
		try {
			ResultSet rs = dataBase.getDropDown(Field.RUNWAY_IDENTIFIER
					.toString().toUpperCase(), aircraftId);
			ArrayList<String> al = new ArrayList<>();

			while (rs.next()) {
				al.add(rs.getString(1));
			}

			ids = al.toArray(new String[al.size()]);
		} catch (SQLException e) {
			showError(e);
			return null;
		}

		return ids;
	}

	private String getRunwayId() {
		String id;

		// Get the runway id from the component if the component is ready
		JComboBox<Object> jcb = cmap.getComboBox(Field.RUNWAY_IDENTIFIER);
		if (jcb != null && jcb.getSelectedItem() != null)
			return cmap.getComboBox(Field.RUNWAY_IDENTIFIER).getSelectedItem()
					.toString();

		// Otherwise get it from the last used settings
		else {

			return settings.getStringForKey(Settings.LAST_RUNWAY);
		}
	}

	private String getAircraftId() {
		String id;

		// Get the runway id from the component if the component is ready
		JComboBox<Object> jcb = cmap.getComboBox(Field.AIRCRAFT_IDENTIFIER);
		if (jcb != null && jcb.getSelectedItem() != null)
			return cmap.getComboBox(Field.AIRCRAFT_IDENTIFIER)
					.getSelectedItem().toString();
		else {
			// Otherwise get it from the last used settings

			return settings.getStringForKey(Settings.LAST_AIRCRAFT);
		}
	}

	private String getField(Field key) {

		try {
			ResultSet r = dataBase.getField(key.toString().toUpperCase(),
					getAircraftId(), getRunwayId());
			if (r.next()) {
				String s = r.getString(1);
				if (s != null)
					return s;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	private void selectImage(JLabel label) {
		File image = showImageChooser();

		if (image != null) {
			try {
				image = Repository.copyImageFile(runway, image);
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			runway.put(Field.IMAGE_PATH, image.getAbsolutePath());
			if (runway.isModified()) {
				runway.save();
			}

			loadImage(image.getAbsolutePath(), label);

		}
	}

	private void loadImage(String path, JLabel label) {
		ImageIcon thumbnail;
		ImageIcon tmpIcon = new ImageIcon(path);
		if (tmpIcon != null) {
			if (tmpIcon.getIconWidth() > 430) {
				thumbnail = new ImageIcon(tmpIcon.getImage().getScaledInstance(
						430, 380, Image.SCALE_DEFAULT));
			} else { // no need to miniaturize
				thumbnail = tmpIcon;
			}
			label.setIcon(thumbnail);
		}
	}

	private File showImageChooser() {
		FileFilter imageFilter = new FileFilter() {

			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				}

				String extention = getExtension(f);

				if (extention != null
						&& (extention.equals("jpg") || extention.equals("png"))) {
					return true;
				}

				return false;
			}

			public String getDescription() {
				return "Image Files (*.jpg, *.png)";
			}
		};

		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Select New Image File");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setFileFilter(imageFilter);
		chooser.setAccessory(new ImagePreview(chooser));

		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile();
		} else {
			return null;
		}
	}

	public static String getExtension(File f) {
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');

		if (i > 0 && i < s.length() - 1) {
			ext = s.substring(i + 1).toLowerCase();
		}
		return ext;
	}

	private void popupCalendar() {

		JCalendar cal = new JCalendar(new Date());

		cal.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("calendar".equals(evt.getPropertyName())) {
					String date = ((GregorianCalendar) evt.getNewValue())
							.getTime().toString();
					((JTextField) cmap.getComponent(Field.INSPECTION_DUE))
							.setText((date));
					runway.put(Field.INSPECTION_DUE, date);
					runway.save();

				}
			}
		});

		JDialog d = new JDialog(this);
		d.getContentPane().add(cal);
		d.pack();
		d.setLocationRelativeTo(this);
		d.setVisible(true);
	}

	void loadLabels() {
		// load labels
		for (Entry<JLabel, Field> e : labels.entrySet()) {
			e.getKey().setText(settings.getLabel(e.getValue()));
		}

		// add units to labels
		for (Entry<JLabel, Settings> e : labelunits.entrySet()) {
			String l = e.getKey().getText();
			l += " (";
			l += settings.getStringForKey(e.getValue());
			l += ')';
			e.getKey().setText(l);
		}
	}

	void loadData(boolean initialize) {
		// load new runway
		loadRunway();

		// load text components
		for (Entry<JTextComponent, Field> e : textComponents.entrySet()) {
			e.getKey().setText(getField(e.getValue()));
			if (initialize)
				Save.onChange(e.getKey(), e.getValue());
		}

		// load check boxes
		for (Entry<JCheckBox, Field> e : checkBoxes.entrySet()) {
			e.getKey()
					.setSelected(Boolean.parseBoolean(getField(e.getValue())));
			if (initialize)
				Save.onChange(e.getKey(), e.getValue());

		}

		// load combo boxes
		for (Entry<JComboBox<Object>, Field> e : comboBoxes.entrySet()) {
			JComboBox<Object> jcb = e.getKey();
			Field f = e.getValue();

			jcb.setModel(new JJDefaultsComboBoxModel(jcb, f));
			jcb.setSelectedItem(getField(f));
			if (initialize)
				Save.onChange(e.getKey(), e.getValue());

		}

		// load image
		loadImage(getField(Field.IMAGE_PATH), lblImage);
	}

	void centerWindow() {
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		int x = (int) (width * .95);
		int y = (int) (height * .9);
		int xpos = (width - x) / 2;
		setBounds(xpos, 0, x, y);
	}

	/**
	 * 
	 * Used to specify the direction the category will place components.
	 * 
	 */
	public enum Direction {
		DOWN, ACROSS
	}

	/**
	 * 
	 * A JPanel with labels fields and highlight check boxes.
	 * 
	 */
	private class Category extends JPanel implements GridBagComponent {
		GridBagConstraints constraints;
		Direction direction = Direction.DOWN;
		boolean highlight = true;

		public Category() {
			setLayout(new GridBagLayout());
			if (highlight)
				addHighlightLabel();
		}

		public Category(String name) {
			this();
			setBorder(BorderFactory.createTitledBorder(name));
		}

		public Category(String name, boolean highlight) {
			this.highlight = highlight;
			setLayout(new GridBagLayout());
			setBorder(BorderFactory.createTitledBorder(name));
			if (highlight)
				addHighlightLabel();
		}

		public void add(java.util.List<Pair<Field, Field>> fieldHighlight) {
			for (int i = 0; i < fieldHighlight.size(); i++) {
				GridBagConstraints l = new GridBagConstraints();
				GridBagConstraints g = new GridBagConstraints();

				if (direction == Direction.DOWN) {
					l.gridx = 0;
					g.gridx = 1;
					g.gridy = l.gridy = i + 1;
				} else {
					l.gridx = 2 * i;
					g.gridx = 2 * i + 1;
					g.gridy = l.gridy = 0;
				}

				g.weightx = 1;
				g.fill = GridBagConstraints.HORIZONTAL;

				l.insets = g.insets = new Insets(5, 5, 5, 5);

				add(cmap.get(fieldHighlight.get(i).a).getLabel(), l);
				add(cmap.getComponent(fieldHighlight.get(i).a), g);

				if (highlight && fieldHighlight.get(i).b != null) {
					GridBagConstraints gh = new GridBagConstraints();
					gh.gridx = 2;
					gh.gridy = i + 1;
					gh.insets = new Insets(5, 5, 5, 5);
					add(cmap.getComponent(fieldHighlight.get(i).b), gh);
				}
			}
		}

		private void addHighlightLabel() {
			JLabel h = new JLabel("Highlight");
			GridBagConstraints g = new GridBagConstraints();
			g.gridx = 2;
			g.gridy = 0;
			add(h, g);
		}

		@Override
		public GridBagConstraints getGridbaBagConstraints() {
			if (constraints == null) {
				constraints = new GridBagConstraints();
			}

			return constraints;
		}
	}

	private class Pair<A, B> {
		public final A a;
		public final B b;

		public Pair(A a, B b) {
			this.a = a;
			this.b = b;
		}
	}

	/**
	 * Components that store their own constraints.
	 */
	private interface GridBagComponent {
		public GridBagConstraints getGridbaBagConstraints();
	}

	/**
	 * 
	 * Information category on the data tab.
	 * 
	 */
	private class InformationCategory extends Category {

		public InformationCategory() {
			super("Information");

			ArrayList<Pair<Field, Field>> fieldHighlight = new ArrayList<>();

			fieldHighlight.add(new Pair<Field, Field>(Field.CLASSIFICATION,
					null));
			fieldHighlight.add(new Pair<Field, Field>(Field.FREQUENCY_1, null));
			fieldHighlight.add(new Pair<Field, Field>(Field.FREQUENCY_2, null));
			fieldHighlight.add(new Pair<Field, Field>(Field.LANGUAGE_GREET,
					null));
			fieldHighlight.add(new Pair<Field, Field>(
					Field.PRECIPITATION_ON_SCREEN,
					Field.PRECIPITATION_ON_SCREEN_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.ELEVATION,
					Field.ELEVATION_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.LENGTH,
					Field.LENGTH_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.WIDTH_TEXT,
					Field.WIDTH_TEXT_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.TDZ_SLOPE,
					Field.TDZ_SLOPE_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.IAS_ADJUSTMENT,
					Field.IAS_ADJUSTMENT_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.LONGITUDE, null));
			fieldHighlight.add(new Pair<Field, Field>(Field.LATITUDE, null));

			add(fieldHighlight);

			getGridbaBagConstraints().gridx = 0;
			getGridbaBagConstraints().gridy = 2;
			getGridbaBagConstraints().fill = GridBagConstraints.HORIZONTAL;
			getGridbaBagConstraints().weightx = 1;
		}
	}

	private class InspectionCategory extends Category {
		public InspectionCategory() {
			super("Inspection", false);
			super.direction = Direction.ACROSS;

			ArrayList<Pair<Field, Field>> fieldHighlight = new ArrayList<>();
			fieldHighlight
					.add(new Pair<Field, Field>(Field.INSPECTION_NA, null));
			fieldHighlight.add(new Pair<Field, Field>(Field.INSPECTOR_NAME,
					null));
			fieldHighlight.add(new Pair<Field, Field>(Field.INSPECTION_DATE,
					null));
			fieldHighlight.add(new Pair<Field, Field>(Field.INSPECTION_DUE,
					null));

			add(fieldHighlight);

			JButton btnInspectionDue = new JButton("Select");
			GridBagConstraints gbc_btnInspectionDue = new GridBagConstraints();
			add(btnInspectionDue, gbc_btnInspectionDue);
			btnInspectionDue.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					popupCalendar();

				}
			});

			getGridbaBagConstraints().gridx = 0;
			getGridbaBagConstraints().gridy = 1;
			getGridbaBagConstraints().gridwidth = 4;
			getGridbaBagConstraints().fill = GridBagConstraints.HORIZONTAL;
			getGridbaBagConstraints().weightx = 1;
		}
	}

	private class FieldTextArea extends FieldComponent {
		JTextArea jta;

		public FieldTextArea(Field f) {
			super(f);
			jta = new JTextArea();
			Save.onChange(jta, super.componentField);
			load();
		}

		@Override
		public JComponent getComponent() {

			return jta;
		}

		@Override
		public void load() {
			jta.setText(getField(super.componentField));

		}

	}

	private class RunwaysCategory extends Category {
		public RunwaysCategory() {
			super("Runways");

			ArrayList<Pair<Field, Field>> fieldHighlight = new ArrayList<>();
			fieldHighlight.add(new Pair<Field, Field>(Field.RUNWAY_A,
					Field.RUNWAY_A_HL));
			fieldHighlight
					.add(new Pair<Field, Field>(Field.A_TAKEOFF_RESTRICTION,
							Field.A_TAKEOFF_RESTRICTION_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.A_TAKEOFF_NOTE,
					Field.A_TAKEOFF_NOTE_HL));
			fieldHighlight
					.add(new Pair<Field, Field>(Field.A_LANDING_RESTRICTION,
							Field.A_LANDING_RESTRICTION_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.A_LANDING_NOTE,
					Field.A_LANDING_NOTE_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.RUNWAY_B,
					Field.RUNWAY_B_HL));
			fieldHighlight
					.add(new Pair<Field, Field>(Field.B_TAKEOFF_RESTRICTION,
							Field.B_TAKEOFF_RESTRICTION_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.B_TAKEOFF_NOTE,
					Field.B_TAKEOFF_NOTE_HL));
			fieldHighlight
					.add(new Pair<Field, Field>(Field.B_LANDING_RESTRICTION,
							Field.B_LANDING_RESTRICTION_HL));
			fieldHighlight.add(new Pair<Field, Field>(Field.B_LANDING_NOTE,
					Field.B_LANDING_NOTE_HL));

			add(fieldHighlight);

			getGridbaBagConstraints().gridx = 2;
			getGridbaBagConstraints().gridy = 2;
			getGridbaBagConstraints().gridwidth = 2;
			getGridbaBagConstraints().fill = GridBagConstraints.HORIZONTAL;
			getGridbaBagConstraints().weightx = 1;
		}
	}

	private class Tab extends JScrollPane {
		private JTabbedPane tp;
		private String name;
		private JPanel jp;

		public Tab() {
			name = "";
			jp = new JPanel(new GridBagLayout());
			setViewportView(jp);
		}

		public void setJTabbedPane(JTabbedPane tabbedPane) {
			tp = tabbedPane;
			tp.addTab(name, this);
		}

		private void refreshName() {
			tp.setTitleAt(tp.indexOfComponent(this), name);
		}

		public void resetScrollBar() {
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					getVerticalScrollBar().setValue(0);
				}
			});
		}
	}

	private class DataTab extends Tab {

		public DataTab() {
			setName();
			addDisclaimer();
			addInspection();
			addInformation();
			addRunways();
			addImage();
			addChangeLog();
			addButtons();
		}

		private void setName() {
			super.name = getRunwayId() + " DATA";
		}

		public void refreshTitle() {
			setName();
			super.refreshName();
		}

		private void addInspection() {
			InspectionCategory ic = new InspectionCategory();
			super.jp.add(ic, ic.getGridbaBagConstraints());
		}

		private void addDisclaimer() {
			JLabel jl = new JLabel("aircraft disclaimer");
			jl.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints g = new GridBagConstraints();
			g.gridwidth = 4;
			g.gridx = 0;
			g.gridy = 0;
			super.jp.add(jl, g);
		}

		private void addInformation() {
			InformationCategory ic = new InformationCategory();
			super.jp.add(ic, ic.getGridbaBagConstraints());
		}

		private void addRunways() {
			RunwaysCategory rc = new RunwaysCategory();
			super.jp.add(rc, rc.getGridbaBagConstraints());
		}

		private void addImage() {
			JPanel pnlImage = new JPanel();
			pnlImage.setBorder(new TitledBorder(null, "Image",
					TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_pnlImage = new GridBagConstraints();
			gbc_pnlImage.gridwidth = 2;
			gbc_pnlImage.insets = new Insets(5, 5, 5, 5);
			gbc_pnlImage.fill = GridBagConstraints.BOTH;
			gbc_pnlImage.gridx = 0;
			gbc_pnlImage.gridy = 4;
			super.jp.add(pnlImage, gbc_pnlImage);
			GridBagLayout gbl_pnlImage = new GridBagLayout();
			gbl_pnlImage.columnWidths = new int[] { 0, 0 };
			gbl_pnlImage.rowHeights = new int[] { 0, 0, 0, 0, 0 };
			gbl_pnlImage.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
			gbl_pnlImage.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0,
					Double.MIN_VALUE };
			pnlImage.setLayout(gbl_pnlImage);

			JButton btnSelectNewImage = new JButton("Select New Image File");
			GridBagConstraints gbc_btnSelectNewImage = new GridBagConstraints();
			gbc_btnSelectNewImage.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnSelectNewImage.insets = new Insets(5, 5, 5, 0);
			gbc_btnSelectNewImage.gridx = 0;
			gbc_btnSelectNewImage.gridy = 2;
			pnlImage.add(btnSelectNewImage, gbc_btnSelectNewImage);

			btnSelectNewImage.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					selectImage(lblImage);
				}
			});

			lblImage = new JLabel();
			GridBagConstraints gbc_lblImage = new GridBagConstraints();
			gbc_lblImage.insets = new Insets(5, 5, 5, 0);
			gbc_lblImage.fill = GridBagConstraints.BOTH;
			gbc_lblImage.gridx = 0;
			gbc_lblImage.gridy = 1;
			pnlImage.add(lblImage, gbc_lblImage);
			GridBagConstraints gbc_panel_2_1 = new GridBagConstraints();
			gbc_panel_2_1.insets = new Insets(0, 0, 5, 0);
			gbc_panel_2_1.anchor = GridBagConstraints.NORTHWEST;
			gbc_panel_2_1.gridx = 1;
			gbc_panel_2_1.gridy = 0;

			// set up image
			loadImage(getField(Field.IMAGE_PATH), lblImage);
		}

		private void addChangeLog() {
			JPanel panel_8 = new JPanel();
			panel_8.setBorder(new TitledBorder(null, "Change Log",
					TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel_8 = new GridBagConstraints();
			gbc_panel_8.gridwidth = 2;
			gbc_panel_8.insets = new Insets(5, 5, 5, 5);
			gbc_panel_8.fill = GridBagConstraints.BOTH;
			gbc_panel_8.gridx = 2;
			gbc_panel_8.gridy = 4;
			super.jp.add(panel_8, gbc_panel_8);
			GridBagLayout gbl_panel_8 = new GridBagLayout();
			gbl_panel_8.columnWidths = new int[] { 0, 0 };
			gbl_panel_8.rowHeights = new int[] { 0, 0 };
			gbl_panel_8.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
			gbl_panel_8.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
			panel_8.setLayout(gbl_panel_8);

			txtChangeLog = new JTextArea();
			GridBagConstraints gbc_textArea_4 = new GridBagConstraints();
			gbc_textArea_4.insets = new Insets(5, 5, 5, 5);
			gbc_textArea_4.fill = GridBagConstraints.BOTH;
			gbc_textArea_4.gridx = 0;
			gbc_textArea_4.gridy = 0;
			panel_8.add(txtChangeLog, gbc_textArea_4);
		}

		private void addButtons() {
			JButton btnNewButton = new JButton("Publish / Unavailable");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						runway.publish();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
			gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
			gbc_btnNewButton.gridx = 2;
			gbc_btnNewButton.gridy = 5;
			super.jp.add(btnNewButton, gbc_btnNewButton);

			JButton btnPreviewView = new JButton("Preview / View");
			btnPreviewView.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					File f = runway.preview();

					try {
						java.awt.Desktop.getDesktop().open(f);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});
			GridBagConstraints gbc_btnPreviewView = new GridBagConstraints();
			gbc_btnPreviewView.insets = new Insets(0, 0, 0, 5);
			gbc_btnPreviewView.gridx = 3;
			gbc_btnPreviewView.gridy = 5;
			super.jp.add(btnPreviewView, gbc_btnPreviewView);
		}
	}

	private class SettingsTab extends Tab {
		public SettingsTab() {
			super.name = "Settings";
			addGeneralCategory();
		}

		private void addGeneralCategory() {
			SettingsCategory sc = new SettingsCategory();
			sc.setName("General");
			sc.add(Settings.IS_PRIMARY);
			sc.add(Settings.REPOSITORY_PATH);
			sc.add(Settings.PRIMARY_JDBC_URI);
			sc.add(Settings.PRIMARY_JDBC_CLASS_PATH);
			sc.add(Settings.ALTITUDE_UNITS);
			sc.add(Settings.WEIGHT_UNITS);
			sc.add(Settings.DISTANCE_UNITS);
			sc.add(Settings.DISTANCE_CONVERT_FACTOR);
			sc.add(Settings.ADMIN_EMAIL);
			sc.add(Settings.ADMIN_PASSWORD);
			sc.add(Settings.LONG_LAT_FORMAT);
			sc.add(Settings.IS_TRUE_COURSE);
			sc.add(Settings.MAGNETIC_VARIATION);
			GridBagConstraints g = new GridBagConstraints();
			g.weightx = 1;
			g.fill = GridBagConstraints.HORIZONTAL;
			g.insets = new Insets(5, 5, 5, 5);
			super.jp.add(sc, g);
		}
	}

	private abstract class SettingsComponent {
		private HashMap<Settings, String> labels;
		JLabel label;

		public SettingsComponent(Settings s) {
			fillLabels();
			setLabel(s);
		}

		private void setLabel(Settings s) {
			label = new JLabel(getLabel(s));
		}

		public JLabel getJLabel() {
			return label;
		}

		private void fillLabels() {
			labels = new HashMap<>();
			labels.put(Settings.IS_PRIMARY, new String("Primary Setup"));
			labels.put(Settings.REPOSITORY_PATH, new String("Repository Path"));
			labels.put(Settings.PRIMARY_JDBC_URI, new String(
					"Primary Database URI"));
			labels.put(Settings.PRIMARY_JDBC_CLASS_PATH, new String(
					"Primary Database Class Path"));
			labels.put(Settings.IS_OPERATIONS_DB, new String(""));
			labels.put(Settings.OPERATIONS_JDBC_URI, new String(""));
			labels.put(Settings.OPERATIONS_JDBC_CLASS_PATH, new String(""));
			labels.put(Settings.OPERATIONS_TABLE_NAME, new String(""));
			labels.put(Settings.ADMIN_PASSWORD, new String("Admin Password"));
			labels.put(Settings.LAST_RUNWAY, new String(""));
			labels.put(Settings.LAST_AIRCRAFT, new String(""));
			labels.put(Settings.ADMIN_EMAIL, new String("Admin Email"));
			labels.put(Settings.ALTITUDE_UNITS, new String("Altitude Units"));
			labels.put(Settings.WEIGHT_UNITS, new String("Weight Units"));
			labels.put(Settings.DISTANCE_UNITS, new String("Distance Units"));
			labels.put(Settings.DIMENSION_UNITS, new String("Dimension Units"));
			labels.put(Settings.DISTANCE_CONVERT_FACTOR, new String(
					"Distance Conversion Factor"));
			labels.put(Settings.LONG_LAT_FORMAT, new String("Long/Lat Format"));
			labels.put(Settings.IS_TRUE_COURSE, new String("Use True Course"));
			labels.put(Settings.MAGNETIC_VARIATION, new String(
					"Magnetic Variation"));
			labels.put(Settings.HOME_RUNWAY, new String("Home Runway"));
			labels.put(Settings.SECONDARY_RUNWAY,
					new String("Secondary Runway"));
			labels.put(Settings.PAGE_1_DISCLAIMER, new String(
					"Page 1 Disclamer"));
			labels.put(Settings.PAGE_2_DISCLAIMER, new String(
					"Page 2 Disclamer"));
			labels.put(Settings.DEFAULT_EXPIRATION_PERIOD, new String(
					"Default Expiration (Days)"));
			labels.put(Settings.WEB_ROOT, new String(""));
		}

		private String getLabel(Settings s) {
			return labels.get(s);
		}

		public abstract JComponent getComponent();
	}

	private class SettingsCheckBox extends SettingsComponent {
		JCheckBox cb;

		public SettingsCheckBox(final Settings s) {
			super(s);
			cb = new JCheckBox();
			set(s);
			new OnChange(cb) {

				@Override
				public void onChange(String value) {
					saveSetting(s);
				}
			};
		}

		private void set(Settings s) {
			cb.setSelected(settings.getBooleanForKey(s));
		}

		private void saveSetting(Settings s) {
			settings.setValue(s, Boolean.toString(cb.isSelected()));
		}

		@Override
		public JComponent getComponent() {
			return cb;
		}
	}

	private class settingsFileChooser extends SettingsComponent {
		JFileChooser fc = new JFileChooser();
		JPanel p = new JPanel(new GridBagLayout());
		JButton b = new JButton("Browse");
		JTextField tf = new JTextField();

		public settingsFileChooser(final Settings s) {
			super(s);
			get(s);
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			b.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						set(s, fc.getSelectedFile().getAbsolutePath());
						get(s);
					}
				}
			});
			new OnChange(tf) {

				@Override
				public void onChange(String value) {
					set(s, value);
				}
			};
			addComponents();
		}

		@Override
		public JComponent getComponent() {
			return p;
		}

		private void get(Settings s) {
			tf.setText(settings.get(s));
		}

		private void set(Settings s, String path) {
			settings.setValue(s, path);
		}

		private void addComponents() {
			GridBagConstraints tfc = new GridBagConstraints();
			GridBagConstraints bc = new GridBagConstraints();
			tfc.fill = GridBagConstraints.HORIZONTAL;
			tfc.weightx = 1;
			bc.gridx = 1;
			p.add(tf, tfc);
			p.add(b, bc);
		}

	}

	private class SettingsTextField extends SettingsComponent {
		private JTextField tf = new JTextField();

		public SettingsTextField(final Settings s) {
			super(s);
			get(s);
			new OnChange(tf) {

				@Override
				public void onChange(String value) {
					set(s, value);

				}
			};
		}

		@Override
		public JComponent getComponent() {
			return tf;
		}

		private void get(Settings s) {
			tf.setText(settings.get(s));
		}

		private void set(Settings s, String value) {
			settings.setValue(s, value);
		}

	}

	private class SettingsDistance extends SettingsComponent {
		private JComboBox<Object> cb = new JComboBox<>();
		final String[] distanceUnits = { "ft", "m", "y", "km", "mi", "nm" };

		public SettingsDistance(final Settings s) {
			super(s);
			for (String unit : distanceUnits)
				cb.addItem(unit);
			get(s);
			new OnChange(cb) {

				@Override
				public void onChange(String value) {
					set(s, value);
				}
			};
		}

		@Override
		public JComponent getComponent() {
			return cb;
		}

		private void get(Settings s) {
			cb.setSelectedItem(settings.get(s));
		}

		private void set(Settings s, String unit) {
			settings.setValue(s, unit);
		}

	}

	private class SettingsWeight extends SettingsComponent {
		private JComboBox<Object> cb = new JComboBox<>();
		final String[] distanceUnits = { "lbs", "kg" };

		public SettingsWeight(final Settings s) {
			super(s);
			for (String unit : distanceUnits)
				cb.addItem(unit);
			get(s);
			new OnChange(cb) {

				@Override
				public void onChange(String value) {
					set(s, value);
				}
			};
		}

		@Override
		public JComponent getComponent() {
			return cb;
		}

		private void get(Settings s) {
			cb.setSelectedItem(settings.get(s));
		}

		private void set(Settings s, String unit) {
			settings.setValue(s, unit);
		}
	}

	private class SettingsCoordinateFormat extends SettingsComponent {
		private JComboBox<Object> cb = new JComboBox<>();
		final String[] distanceUnits = { "DMS", "Decimal" };

		public SettingsCoordinateFormat(final Settings s) {
			super(s);
			for (String unit : distanceUnits)
				cb.addItem(unit);
			get(s);
			new OnChange(cb) {

				@Override
				public void onChange(String value) {
					set(s, value);
				}
			};
		}

		@Override
		public JComponent getComponent() {
			return cb;
		}

		private void get(Settings s) {
			cb.setSelectedItem(settings.get(s));
		}

		private void set(Settings s, String unit) {
			settings.setValue(s, unit);
		}
	}

	private class SettingsCategory extends JPanel {
		private int row;

		public void setName(String s) {
			setBorder(BorderFactory.createTitledBorder(s));
		}

		public SettingsCategory() {
			setLayout(new GridBagLayout());
		}

		public void add(Settings s) {
			GridBagConstraints lc = new GridBagConstraints();
			GridBagConstraints cc = new GridBagConstraints();
			cc.gridy = lc.gridy = row;
			lc.gridx = 0;
			cc.gridx = 1;
			cc.fill = GridBagConstraints.HORIZONTAL;
			cc.weightx = 1;
			cc.insets = lc.insets = new Insets(5, 5, 5, 5);

			add(cmap.get(s).getJLabel(), lc);
			add(cmap.get(s).getComponent(), cc);
			row++;
		}
	}

	/**
	 * Create the frame.
	 */
	public NewUI() {
		labels = new HashMap<JLabel, Field>();
		labelunits = new HashMap<JLabel, Settings>();
		textComponents = new HashMap<JTextComponent, Field>();
		checkBoxes = new HashMap<JCheckBox, Field>();
		comboBoxes = new HashMap<>();

		settings = DatabaseManager.getSettings();
		cmap = new ComponentMap();
		db = DatabaseManager.getDatabase();
		dataBase = new DataBaseAccess("sqlite", "JJDB(new) - Copy.sqlite");
        Save.setDBA(dataBase);
        Save.setAircraft(getAircraftId());
        Save.setRunway(getRunwayId());

		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				formWindowClosing(evt);
			}
		});

		setTitle("Jungle Jepps - Desktop Application");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		centerWindow();

		// Setup main panel
		JPanel panel = new JPanel();
		setContentPane(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		// label for main window
		JLabel lblJungleJeppsDesktop = new JLabel("Jungle Jepps Desktop");
		GridBagConstraints gbc_lblJungleJeppsDesktop = new GridBagConstraints();
		gbc_lblJungleJeppsDesktop.gridwidth = 4;
		panel.add(lblJungleJeppsDesktop, gbc_lblJungleJeppsDesktop);

		// run way selector
		GridBagConstraints gbc_lblRunwayIdentifier = new GridBagConstraints();
		gbc_lblRunwayIdentifier.gridy = 2;
		gbc_lblRunwayIdentifier.gridx = 2;
		gbc_lblRunwayIdentifier.weightx = 0;
		panel.add(cmap.get(Field.RUNWAY_IDENTIFIER).getLabel(),
				gbc_lblRunwayIdentifier);

		GridBagConstraints gbc_runwayComboBox = new GridBagConstraints();
		gbc_runwayComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_runwayComboBox.weightx = 1;
		gbc_runwayComboBox.gridx = 3;
		gbc_runwayComboBox.gridy = 2;
		panel.add(cmap.getComponent(Field.RUNWAY_IDENTIFIER),
				gbc_runwayComboBox);

		// aircraft selector
		GridBagConstraints gbc_lblAircraftType = new GridBagConstraints();
		gbc_lblAircraftType.weightx = 0;
		gbc_lblAircraftType.gridx = 0;
		gbc_lblAircraftType.gridy = 2;
		panel.add(cmap.get(Field.AIRCRAFT_IDENTIFIER).getLabel(),
				gbc_lblAircraftType);

		GridBagConstraints gbc_cmbbxAltitudeUnits1 = new GridBagConstraints();
		gbc_cmbbxAltitudeUnits1.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxAltitudeUnits1.weightx = 1;
		gbc_cmbbxAltitudeUnits1.gridx = 1;
		gbc_cmbbxAltitudeUnits1.gridy = 2;
		panel.add(cmap.getComponent(Field.AIRCRAFT_IDENTIFIER),
				gbc_cmbbxAltitudeUnits1);

		// tab pane
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.weighty = 1;
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridwidth = 4;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 3;
		panel.add(tabbedPane, gbc_tabbedPane);

		dataTab = new DataTab();
		dataTab.setJTabbedPane(tabbedPane);

		SettingsTab settingsTab = new SettingsTab();
		settingsTab.setJTabbedPane(tabbedPane);

		JPanel pnlSettings = new JPanel();
		GridBagLayout gbl_pnlSettings = new GridBagLayout();
		pnlSettings.setLayout(gbl_pnlSettings);

		JScrollPane scrllPnSettings = new JScrollPane();
		tabbedPane.addTab("Settings", null, scrllPnSettings, null);
		scrllPnSettings.setViewportView(pnlSettings);

		JPanel panel_2 = new JPanel();
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		panel_2.setLayout(gbl_panel_2);

		JLabel lblIsPrimary = new JLabel("Primary Setup");
		GridBagConstraints gbc_lblIsPrimary = new GridBagConstraints();
		gbc_lblIsPrimary.anchor = GridBagConstraints.EAST;
		gbc_lblIsPrimary.insets = new Insets(5, 5, 5, 5);
		gbc_lblIsPrimary.gridx = 0;
		gbc_lblIsPrimary.gridy = 0;
		panel_2.add(lblIsPrimary, gbc_lblIsPrimary);

		JCheckBox chckbxIsPrimary = new JCheckBox("");
		GridBagConstraints gbc_chckbxIsPrimary = new GridBagConstraints();
		gbc_chckbxIsPrimary.insets = new Insets(5, 5, 5, 5);
		gbc_chckbxIsPrimary.gridx = 1;
		gbc_chckbxIsPrimary.gridy = 0;
		panel_2.add(chckbxIsPrimary, gbc_chckbxIsPrimary);

		JLabel lblRepositoryPath = new JLabel("Repository Path");
		GridBagConstraints gbc_lblRepositoryPath = new GridBagConstraints();
		gbc_lblRepositoryPath.anchor = GridBagConstraints.EAST;
		gbc_lblRepositoryPath.insets = new Insets(5, 5, 5, 5);
		gbc_lblRepositoryPath.gridx = 0;
		gbc_lblRepositoryPath.gridy = 1;
		panel_2.add(lblRepositoryPath, gbc_lblRepositoryPath);

		txtRepositoryPath = new JTextField();
		GridBagConstraints gbc_txtRepositoryPath = new GridBagConstraints();
		gbc_txtRepositoryPath.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRepositoryPath.insets = new Insets(0, 0, 5, 0);
		gbc_txtRepositoryPath.gridx = 1;
		gbc_txtRepositoryPath.gridy = 1;
		panel_2.add(txtRepositoryPath, gbc_txtRepositoryPath);

		JLabel lblPrimaryJdbcUri = new JLabel("Primary Database URI");
		GridBagConstraints gbc_lblPrimaryJdbcUri = new GridBagConstraints();
		gbc_lblPrimaryJdbcUri.anchor = GridBagConstraints.EAST;
		gbc_lblPrimaryJdbcUri.insets = new Insets(5, 5, 5, 5);
		gbc_lblPrimaryJdbcUri.gridx = 0;
		gbc_lblPrimaryJdbcUri.gridy = 2;
		panel_2.add(lblPrimaryJdbcUri, gbc_lblPrimaryJdbcUri);

		txtPrimaryJdbcUri = new JTextField();
		GridBagConstraints gbc_txtPrimaryJdbcUri = new GridBagConstraints();
		gbc_txtPrimaryJdbcUri.insets = new Insets(0, 0, 5, 0);
		gbc_txtPrimaryJdbcUri.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPrimaryJdbcUri.gridx = 1;
		gbc_txtPrimaryJdbcUri.gridy = 2;
		panel_2.add(txtPrimaryJdbcUri, gbc_txtPrimaryJdbcUri);
		txtPrimaryJdbcUri.setColumns(10);

		JLabel lblPrimaryJdbcClasspath = new JLabel(
				"Primary Database Class Path");
		GridBagConstraints gbc_lblPrimaryJdbcClasspath = new GridBagConstraints();
		gbc_lblPrimaryJdbcClasspath.anchor = GridBagConstraints.EAST;
		gbc_lblPrimaryJdbcClasspath.insets = new Insets(5, 5, 5, 5);
		gbc_lblPrimaryJdbcClasspath.gridx = 0;
		gbc_lblPrimaryJdbcClasspath.gridy = 3;
		panel_2.add(lblPrimaryJdbcClasspath, gbc_lblPrimaryJdbcClasspath);

		txtPrimaryJdbcClassPath = new JTextField();
		GridBagConstraints gbc_txtPrimaryJdbcClassPath = new GridBagConstraints();
		gbc_txtPrimaryJdbcClassPath.insets = new Insets(0, 0, 5, 0);
		gbc_txtPrimaryJdbcClassPath.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPrimaryJdbcClassPath.gridx = 1;
		gbc_txtPrimaryJdbcClassPath.gridy = 3;
		panel_2.add(txtPrimaryJdbcClassPath, gbc_txtPrimaryJdbcClassPath);
		txtPrimaryJdbcClassPath.setColumns(10);

		JLabel lblIsOperationsDb = new JLabel("Altitude Units");
		GridBagConstraints gbc_lblIsOperationsDb = new GridBagConstraints();
		gbc_lblIsOperationsDb.anchor = GridBagConstraints.EAST;
		gbc_lblIsOperationsDb.insets = new Insets(5, 5, 5, 5);
		gbc_lblIsOperationsDb.gridx = 0;
		gbc_lblIsOperationsDb.gridy = 4;
		panel_2.add(lblIsOperationsDb, gbc_lblIsOperationsDb);

		JComboBox cmbbxAltitudeUnits = new JComboBox();
		GridBagConstraints gbc_cmbbxAltitudeUnits = new GridBagConstraints();
		gbc_cmbbxAltitudeUnits.insets = new Insets(0, 0, 5, 0);
		gbc_cmbbxAltitudeUnits.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxAltitudeUnits.gridx = 1;
		gbc_cmbbxAltitudeUnits.gridy = 4;
		panel_2.add(cmbbxAltitudeUnits, gbc_cmbbxAltitudeUnits);

		JLabel lblWeightUnits = new JLabel("Weight Units");
		GridBagConstraints gbc_lblWeightUnits = new GridBagConstraints();
		gbc_lblWeightUnits.anchor = GridBagConstraints.EAST;
		gbc_lblWeightUnits.insets = new Insets(0, 0, 5, 5);
		gbc_lblWeightUnits.gridx = 0;
		gbc_lblWeightUnits.gridy = 5;
		panel_2.add(lblWeightUnits, gbc_lblWeightUnits);

		JComboBox cmbbxWeightUnits = new JComboBox();
		GridBagConstraints gbc_cmbbxWeightUnits1 = new GridBagConstraints();
		gbc_cmbbxWeightUnits1.insets = new Insets(0, 0, 5, 0);
		gbc_cmbbxWeightUnits1.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxWeightUnits1.gridx = 1;
		gbc_cmbbxWeightUnits1.gridy = 5;
		panel_2.add(cmbbxWeightUnits, gbc_cmbbxWeightUnits1);

		JLabel lblDistanceUnits = new JLabel("Distance Units");
		GridBagConstraints gbc_lblDistanceUnits = new GridBagConstraints();
		gbc_lblDistanceUnits.anchor = GridBagConstraints.EAST;
		gbc_lblDistanceUnits.insets = new Insets(0, 0, 5, 5);
		gbc_lblDistanceUnits.gridx = 0;
		gbc_lblDistanceUnits.gridy = 6;
		panel_2.add(lblDistanceUnits, gbc_lblDistanceUnits);

		JComboBox cmbbxDistanceUnits = new JComboBox();
		GridBagConstraints gbc_cmbbxDistanceUnits = new GridBagConstraints();
		gbc_cmbbxDistanceUnits.insets = new Insets(0, 0, 5, 0);
		gbc_cmbbxDistanceUnits.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxDistanceUnits.gridx = 1;
		gbc_cmbbxDistanceUnits.gridy = 6;
		panel_2.add(cmbbxDistanceUnits, gbc_cmbbxDistanceUnits);

		JLabel lblDimensionUnits = new JLabel("Dimension Units");
		GridBagConstraints gbc_lblDimensionUnits = new GridBagConstraints();
		gbc_lblDimensionUnits.anchor = GridBagConstraints.EAST;
		gbc_lblDimensionUnits.insets = new Insets(0, 0, 5, 5);
		gbc_lblDimensionUnits.gridx = 0;
		gbc_lblDimensionUnits.gridy = 7;
		panel_2.add(lblDimensionUnits, gbc_lblDimensionUnits);

		JComboBox cmbbxDimensionUnits = new JComboBox();
		GridBagConstraints gbc_cmbbxDimensionUnits = new GridBagConstraints();
		gbc_cmbbxDimensionUnits.insets = new Insets(0, 0, 5, 0);
		gbc_cmbbxDimensionUnits.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxDimensionUnits.gridx = 1;
		gbc_cmbbxDimensionUnits.gridy = 7;
		panel_2.add(cmbbxDimensionUnits, gbc_cmbbxDimensionUnits);

		JLabel lblDistanceConversionFactor = new JLabel(
				"Distance Conversion Factor");
		GridBagConstraints gbc_lblDistanceConversionFactor = new GridBagConstraints();
		gbc_lblDistanceConversionFactor.anchor = GridBagConstraints.EAST;
		gbc_lblDistanceConversionFactor.insets = new Insets(0, 0, 5, 5);
		gbc_lblDistanceConversionFactor.gridx = 0;
		gbc_lblDistanceConversionFactor.gridy = 8;
		panel_2.add(lblDistanceConversionFactor,
				gbc_lblDistanceConversionFactor);

		txtDistanceConvertFactor = new JTextField();
		GridBagConstraints gbc_txtDistanceConvertFactor = new GridBagConstraints();
		gbc_txtDistanceConvertFactor.insets = new Insets(0, 0, 5, 0);
		gbc_txtDistanceConvertFactor.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDistanceConvertFactor.gridx = 1;
		gbc_txtDistanceConvertFactor.gridy = 8;
		panel_2.add(txtDistanceConvertFactor, gbc_txtDistanceConvertFactor);
		txtDistanceConvertFactor.setColumns(10);

		JLabel lblAdminEmail = new JLabel("Admin Email");
		GridBagConstraints gbc_lblAdminEmail = new GridBagConstraints();
		gbc_lblAdminEmail.anchor = GridBagConstraints.EAST;
		gbc_lblAdminEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblAdminEmail.gridx = 0;
		gbc_lblAdminEmail.gridy = 9;
		panel_2.add(lblAdminEmail, gbc_lblAdminEmail);

		txtAdminEmail = new JTextField();
		GridBagConstraints gbc_txtAdminEmail = new GridBagConstraints();
		gbc_txtAdminEmail.insets = new Insets(0, 0, 5, 0);
		gbc_txtAdminEmail.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAdminEmail.gridx = 1;
		gbc_txtAdminEmail.gridy = 9;
		panel_2.add(txtAdminEmail, gbc_txtAdminEmail);
		txtAdminEmail.setColumns(10);

		JLabel lblAdminPassword = new JLabel("Admin Password");
		GridBagConstraints gbc_lblAdminPassword = new GridBagConstraints();
		gbc_lblAdminPassword.anchor = GridBagConstraints.EAST;
		gbc_lblAdminPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblAdminPassword.gridx = 0;
		gbc_lblAdminPassword.gridy = 10;
		panel_2.add(lblAdminPassword, gbc_lblAdminPassword);

		txtAdminPassword = new JTextField();
		GridBagConstraints gbc_txtAdminPassword = new GridBagConstraints();
		gbc_txtAdminPassword.insets = new Insets(0, 0, 5, 0);
		gbc_txtAdminPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAdminPassword.gridx = 1;
		gbc_txtAdminPassword.gridy = 10;
		panel_2.add(txtAdminPassword, gbc_txtAdminPassword);
		txtAdminPassword.setColumns(10);

		JLabel lblLonglatFormat = new JLabel("Long/Lat Format");
		GridBagConstraints gbc_lblLonglatFormat = new GridBagConstraints();
		gbc_lblLonglatFormat.anchor = GridBagConstraints.EAST;
		gbc_lblLonglatFormat.insets = new Insets(0, 0, 5, 5);
		gbc_lblLonglatFormat.gridx = 0;
		gbc_lblLonglatFormat.gridy = 11;
		panel_2.add(lblLonglatFormat, gbc_lblLonglatFormat);

		JComboBox cmbbxLongLatFormat = new JComboBox();
		GridBagConstraints gbc_cmbbxLongLatFormat = new GridBagConstraints();
		gbc_cmbbxLongLatFormat.insets = new Insets(0, 0, 5, 0);
		gbc_cmbbxLongLatFormat.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxLongLatFormat.gridx = 1;
		gbc_cmbbxLongLatFormat.gridy = 11;
		panel_2.add(cmbbxLongLatFormat, gbc_cmbbxLongLatFormat);

		JLabel lblUseTrueCourse = new JLabel("Use True Course");
		GridBagConstraints gbc_lblUseTrueCourse = new GridBagConstraints();
		gbc_lblUseTrueCourse.anchor = GridBagConstraints.EAST;
		gbc_lblUseTrueCourse.insets = new Insets(0, 0, 5, 5);
		gbc_lblUseTrueCourse.gridx = 0;
		gbc_lblUseTrueCourse.gridy = 12;
		panel_2.add(lblUseTrueCourse, gbc_lblUseTrueCourse);

		JCheckBox chckbxIsTrueCourse = new JCheckBox("");
		GridBagConstraints gbc_chckbxIsTrueCourse = new GridBagConstraints();
		gbc_chckbxIsTrueCourse.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxIsTrueCourse.gridx = 1;
		gbc_chckbxIsTrueCourse.gridy = 12;
		panel_2.add(chckbxIsTrueCourse, gbc_chckbxIsTrueCourse);

		JLabel lblMagneticVariation = new JLabel("Magnetic Variation");
		GridBagConstraints gbc_lblMagneticVariation = new GridBagConstraints();
		gbc_lblMagneticVariation.anchor = GridBagConstraints.EAST;
		gbc_lblMagneticVariation.insets = new Insets(0, 0, 5, 5);
		gbc_lblMagneticVariation.gridx = 0;
		gbc_lblMagneticVariation.gridy = 13;
		panel_2.add(lblMagneticVariation, gbc_lblMagneticVariation);

		txtMagneticVariation = new JTextField();
		GridBagConstraints gbc_txtMagneticVariation = new GridBagConstraints();
		gbc_txtMagneticVariation.insets = new Insets(0, 0, 5, 0);
		gbc_txtMagneticVariation.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtMagneticVariation.gridx = 1;
		gbc_txtMagneticVariation.gridy = 13;
		panel_2.add(txtMagneticVariation, gbc_txtMagneticVariation);
		txtMagneticVariation.setColumns(10);

		JLabel lblHomeRunway = new JLabel("Home Runway");
		GridBagConstraints gbc_lblHomeRunway = new GridBagConstraints();
		gbc_lblHomeRunway.anchor = GridBagConstraints.EAST;
		gbc_lblHomeRunway.insets = new Insets(0, 0, 5, 5);
		gbc_lblHomeRunway.gridx = 0;
		gbc_lblHomeRunway.gridy = 14;
		panel_2.add(lblHomeRunway, gbc_lblHomeRunway);

		JComboBox cmbbxHomeRunway = new JComboBox();
		GridBagConstraints gbc_cmbbxHomeRunway = new GridBagConstraints();
		gbc_cmbbxHomeRunway.insets = new Insets(0, 0, 5, 0);
		gbc_cmbbxHomeRunway.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxHomeRunway.gridx = 1;
		gbc_cmbbxHomeRunway.gridy = 14;
		panel_2.add(cmbbxHomeRunway, gbc_cmbbxHomeRunway);

		JLabel lblSecondaryRunway = new JLabel("Secondary Runway");
		GridBagConstraints gbc_lblSecondaryRunway = new GridBagConstraints();
		gbc_lblSecondaryRunway.anchor = GridBagConstraints.EAST;
		gbc_lblSecondaryRunway.insets = new Insets(0, 0, 5, 5);
		gbc_lblSecondaryRunway.gridx = 0;
		gbc_lblSecondaryRunway.gridy = 15;
		panel_2.add(lblSecondaryRunway, gbc_lblSecondaryRunway);

		JComboBox cmbbxSecondaryRunway = new JComboBox();
		GridBagConstraints gbc_cmbbxSecondaryRunway = new GridBagConstraints();
		gbc_cmbbxSecondaryRunway.insets = new Insets(0, 0, 5, 0);
		gbc_cmbbxSecondaryRunway.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbbxSecondaryRunway.gridx = 1;
		gbc_cmbbxSecondaryRunway.gridy = 15;
		panel_2.add(cmbbxSecondaryRunway, gbc_cmbbxSecondaryRunway);

		JLabel lblDefaultExpirationdays = new JLabel(
				"Default Expiration (Days)");
		GridBagConstraints gbc_lblDefaultExpirationdays = new GridBagConstraints();
		gbc_lblDefaultExpirationdays.anchor = GridBagConstraints.EAST;
		gbc_lblDefaultExpirationdays.insets = new Insets(0, 0, 5, 5);
		gbc_lblDefaultExpirationdays.gridx = 0;
		gbc_lblDefaultExpirationdays.gridy = 16;
		panel_2.add(lblDefaultExpirationdays, gbc_lblDefaultExpirationdays);

		txtDefaultExpirationPeriod = new JTextField();
		GridBagConstraints gbc_txtDefaultExpirationPeriod = new GridBagConstraints();
		gbc_txtDefaultExpirationPeriod.insets = new Insets(0, 0, 5, 0);
		gbc_txtDefaultExpirationPeriod.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDefaultExpirationPeriod.gridx = 1;
		gbc_txtDefaultExpirationPeriod.gridy = 16;
		panel_2.add(txtDefaultExpirationPeriod, gbc_txtDefaultExpirationPeriod);
		txtDefaultExpirationPeriod.setColumns(10);

		JLabel lblPageDisclamer = new JLabel("Page 1 Disclamer");
		GridBagConstraints gbc_lblPageDisclamer = new GridBagConstraints();
		gbc_lblPageDisclamer.anchor = GridBagConstraints.EAST;
		gbc_lblPageDisclamer.insets = new Insets(0, 0, 5, 5);
		gbc_lblPageDisclamer.gridx = 0;
		gbc_lblPageDisclamer.gridy = 17;
		panel_2.add(lblPageDisclamer, gbc_lblPageDisclamer);

		JTextArea txtPage1Disclaimer = new JTextArea();
		GridBagConstraints gbc_txtPage1Disclaimer = new GridBagConstraints();
		gbc_txtPage1Disclaimer.insets = new Insets(0, 0, 5, 0);
		gbc_txtPage1Disclaimer.fill = GridBagConstraints.BOTH;
		gbc_txtPage1Disclaimer.gridx = 1;
		gbc_txtPage1Disclaimer.gridy = 17;
		panel_2.add(txtPage1Disclaimer, gbc_txtPage1Disclaimer);

		JLabel lblPageDisclamer_1 = new JLabel("Page 2 Disclamer");
		GridBagConstraints gbc_lblPageDisclamer_1 = new GridBagConstraints();
		gbc_lblPageDisclamer_1.anchor = GridBagConstraints.EAST;
		gbc_lblPageDisclamer_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblPageDisclamer_1.gridx = 0;
		gbc_lblPageDisclamer_1.gridy = 18;
		panel_2.add(lblPageDisclamer_1, gbc_lblPageDisclamer_1);

		JTextArea txtPage2Disclaimer = new JTextArea();
		GridBagConstraints gbc_txtPage2Disclaimer = new GridBagConstraints();
		gbc_txtPage2Disclaimer.insets = new Insets(0, 0, 5, 0);
		gbc_txtPage2Disclaimer.fill = GridBagConstraints.BOTH;
		gbc_txtPage2Disclaimer.gridx = 1;
		gbc_txtPage2Disclaimer.gridy = 18;
		panel_2.add(txtPage2Disclaimer, gbc_txtPage2Disclaimer);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_2.gridheight = 1;
		gbc_panel_2.gridwidth = 1;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		pnlSettings.add(panel_2, gbc_panel_2);
	}
}
