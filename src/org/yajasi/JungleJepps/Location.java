package org.yajasi.JungleJepps;

public class Location {

	Longitude longitude;
	Latitude latitude;
	
	public Location(Longitude longi, Latitude lati){
		longitude = longi;
		latitude = lati;
	}
	
}
